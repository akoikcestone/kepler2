
/*
	// setup a test orbit.
var plutoOrbit = new KeplerOrbit({
	a: 39.48168677, 
	e: 0.24880766,
	P: 90465, 
	M: KeplerFuncts.getMeanAnomaly(new Date(), 14.86012204 * toRad, 90465),
	O: 110.30347 * toRad,
	w: 224.06676 * toRad,
	i: 17.14175 * toRad
});

// setup a test orbit.
var plutoOrbit2 = new KeplerOrbit({
	a: 39.48168677, 
	e: 0.24880766,
	P: 90465, 
	M: KeplerFuncts.getMeanAnomaly(new Date('December 26, 2008 00:00:00'), 14.86012204 * toRad, 90465),
	O: 110.30347 * toRad,
	w: 224.06676 * toRad,
	i: 17.14175 * toRad
});
*/

// test functions in the test orbit
// test radius function
test( "getr test", function() {
	function getr(a, e, expected) {
		for (var x = 0; x <= 7; x++) {
			if (typeof expected[x] === 'undefined') break;
			var ctr = x * Math.PI / 4;
			var expnum = new Number(expected[x]);
			expnum = expnum.toPrecision(15);
			var expnum0 = new Number(plutoOrbit.getr(a, e, ctr));
			expnum0 = expnum0.toPrecision(15);
			
			equal(expnum0, expnum); //Math.ceil(expected[x]*1e17)/1e17);
		}
	}
	
	// eccentricity: sqrt(1 - b^2 / a^2)
	function gete(a, b) {
		return Math.sqrt(1 - Math.pow(b, 2) / Math.pow(a, 2));
	}	
	// expecteds: @ v = 0, pi/4, pi/2, 3pi/4, pi, 5pi/4, 3/2pi, 7pi/4, 2pi
	
	// circle: r should be constant.
	// a = 1, e = 0
	var expected = [1, 1, 1, 1, 1, 1, 1, 1];
	getr(1, 0, expected);
	
	// ellipse half as tall as wide: r is variable.
	// b = 0.5a
	// a = 1, e = 0.5
	
	var expected = [
		0.13397459621556135323627682924706, 
		0.15505102572168219018027159252941, 
		0.25, 
		0.64494897427831780981972840747059, 
		1.8660254037844386467637231707529,
		0.64494897427831780981972840747059,
		0.25,
		0.15505102572168219018027159252941,
		0.13397459621556135323627682924706
	];
	getr(1, gete(1, 0.5), expected);
});

// test julian date conversion
test('julian date test', function() {

	equal(new Date('January 1, 2000 12:00:00').getJulian(), 2451545.0);
	equal(new Date('June 5, 2012 13:41:18').getJulian(), 2456084.070347222);

});

// test mean anomaly from M0
test('mean anomaly from M0 test', function() {
	function testma(date, M0, P, expected) {
		equal(keplerFuncts.getMeanAnomaly(date, M0, P), expected);
	}
	
	testma(new Date('January 1, 2000 12:00:00'), 18.818 * toRad, 4332.59, 18.818 * toRad); 	// Jupiter M=M0
	testma(new Date('May 15, 2014 12:00:00'), 18.818 * toRad, 4332.59, 1.655977012675929523074713233441);
	
	
});

// getPos tests
test('getPos tests', function() {
	//var pos1 = plutoOrbit2.getPos(plutoOrbit2.elements);
	//var pos2 = plutoOrbit2.getPos2(plutoOrbit2.elements);
	
	//deepEqual(pos1, pos2);

	var testObject = new KeplerOrbit({
		name: 'Test',
		a: 26560.46,
		e: 0.0127851,
		E: 5.62764,
		i: 56.26 * toRad,
		O: 342.08 * toRad,
		w: 179.53 * toRad,
		P: 0
	});
	
	var expectedCoords = [-16611, 15032.66, 13762.11];
	
	deepEqual(testObject.getPos(testObject.el), expectedCoords);
	
});

test('heliocentric distance comparison test (getr, getPos)', function() {
	function dist0(posArr) {
		return dist(0, 0, 0, posArr[0], posArr[1], posArr[2]);
	}
	
	function dist(x0, y0, z0, x1, y1, z1) {
		return Math.sqrt(Math.pow(x1 - x0, 2) + Math.pow(y1 - y0, 2) + Math.pow(z1 - z0, 2));
	}

	var pos = plutoOrbit.getPos(plutoOrbit.elements);
	var r = plutoOrbit.getr();
	
	equal(r, dist0(pos));
	
	var pos = plutoOrbit2.getPos(plutoOrbit2.elements);
	var r = plutoOrbit2.getr();
	//alert(pos + '\nr: ' + r + '\ndist: ' + dist(0, 0, 0, pos[0], pos[1], pos[2]));
	equal(r, dist0(pos));
});

test('distance conversions', function() {
	var distanceTests = [
		[1, 'ly', 'km', 9460730472580.8],
		[1, 'km', 'ly', 1 / 9460730472580.8],
		[1, 'ly', 'au', 63241.077],
		[1, 'au', 'ly', 1 / 63241.077],
		[1, 'au', 'km', 6.68458712e-12],
		[1, 'km', 'au', 1 / 6.68458712e-12],
		[1, 'km', 'mi', 0.621371192],
		[1, 'mi', 'km', 1 / 0.621371192]
	];
	
	for (var x = 0; x < distanceTests.length; x++) {
		var el = distanceTests[x];
		equal(keplerFuncts.conv(el[0], el[1], el[2], -1), el[3]);
	}
});