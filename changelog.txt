2014-09-01	Alex Koik-Cestone	<akoikcestone@gmail.com>

* index.html: Improve simulation increment slider

* scripts/KeplerUI.js ($('#siminc-slider').slider.change()): new function. update on programmatic change/reset of slider

* scripts/ThreedFuncts.js (ThreedFuncts.animate): fix simulation speed issue
fix 'fixed date' deselection issue



2014-09-01	Alex Koik-Cestone	<akoikcestone@gmail.com>

* index.html: add planet info box html

* scripts/KeplerFuncts.js (KeplerFuncts.getTemp): new function. get eq temperature 
better for extrasolar systems
(KeplerFuncts.getEqTemp): get eq temperature using solar constant reference
better for 'Sol' solar system

* scripts/KeplerOrbit.js (KeplerOrbit.addEvent): moved popup code into KeplerSystems

* scripts/KeplerSettings.js: added some radius/mass reference values

* scripts/KeplerSystems.js (KeplerSystems.getSysData): new function for retrieving system 
data
(KeplerSystems.planetInfoBox): generate info box for a planet on click
(KeplerSystems.planetInfoBox.show): show the HTML info box
(KeplerSystems.planetInfoBox.hide): hide the HTML info box
(KeplerSystems.planetInfoBox.isHidden): check HTML info box hidden status. unimplemented
(KeplerSystems.planetInfoBox.populate): generate HTML-formatted planetary data for info 
box and populate

* scripts/systems/51pegasi.js (SystemsData.51pegasi): Simplify radius values
Add T value for star

* scripts/systems/gliese676a.js (SystemsData.gliese676a): Add T value for star

* scripts/systems/sol.js (SystemsData.sol): Add temp, solar constant values for star
Add 'greenhouse' temperature shift values for planets (really just a correction factor)

* styles/kepler2-ui-drinfopanel.css: css for transparent-backed boxes is now general

* styles/kepler2-ui-planetinfobox.css: new file. add css for planet info box

* styles/kepler2-ui.css: import planet info box css file



2014-08-30	Alex Koik-Cestone	<akoikcestone@gmail.com>

* index.html (world.constructor): added initial camera radius
incline camera correctly based on radius
add trackball/orbit controls option
add zoom option
add simulation speed option
(anim): new function. allows greater control over animation loop

* scripts/KeplerFuncts.js (KeplerFuncts.toHexColor): new function
(KeplerFuncts.getApsis): new function. get peri/apoapsis

* scripts/KeplerOrbit.js (KeplerOrbit.drawObject): add (basic) onclick 'info' event for 
planets
(KeplerOrbit.addEvent): new function. add onclick 'info' event for planets

* scripts/KeplerSettings.js: add earth radius, sol radius sol mass for use with systems
(KeplerSettings.constructor): add background, foreground color options for world
add simulation increment, max sim rate in FPS (speed control)
add initial camera radius
add inclined view incline option

* scripts/KeplerSystems.js (KeplerSystems.moveToPickedDate): moving to picked date now 
correctly shows updated date in info bar

* scripts/KeplerUI.js: add simulation speed slider bar

* scripts/OrbitControl.js: add ability to programmatically alter theta, phi angles
(OrbitControls.update): checks for user or programmatic changes to angles

* scripts/ThreedFuncts.js (ThreedFuncts.updSimInc): new function. change simulation speed.
(ThreedFuncts.animate0): new function. old animation function
(ThreedFuncts.animate): now runs at set framerate/speed. speeding/slowing process still 
bugged.
(ThreedFuncts.chgView): now manipulates modified OrbitControls directly to change view
(ThreedFuncts.zoom): removed old function
(ThreedFuncts.zoom): uses tweening and OrbitControls

* scripts/systems/kepler186.js: new file. Kepler-186 system

* scripts/systems/sol.js: add vesta 
add (some) descriptions of worlds

* scripts/systems/systems.js: add Kepler-186 system to list



2014-08-09	Alex Koik-Cestone	<akoikcestone@gmail.com>

* index.html (world.constructor): changed some parameters to accommodate tweening (still 
debugging)
added option to enable/disable world controls

* scripts/EllipseLib.js (EllipseLib.arcLen2): made code more concise

* scripts/KeplerFuncts.js (KeplerFuncts.loadDebug): fixed scope bug
(KeplerFuncts.delimit): now automatically rounds down miniscule decimals

* scripts/KeplerOrbit.js (KeplerOrbit.getArcLength): added more debugging info
(KeplerOrbit.addPatialOrbit): Made orbitInfo more legible by separating out functions
numbers are delimited

* scripts/KeplerSettings.js (constructor): can enable or disable world controls
can toggle world axis helper with debug.scene.showWorldAxisHepler

* scripts/KeplerSystems.js (KeplerSystems.addPartialsFromUser): numbers are delimited

* scripts/ThreedFuncts.js (ThreedFuncts.chgView): completely changed to accommodate tweening
some debugging needed to enable smooth camera rotation for all transitions

* scripts/tween.min.js: New script for tweening animation



2014-08-08	Alex Koik-Cestone	<akoikcestone@gmail.com>

* scripts/EllipseLib.js (EllipseLib.arcLen): Fixed bug in calculations
(EllipseLib.arcLen2): New function.
(EllipseLib.circApprox): Formula is now in KeplerSettings

* scripts/KeplerFuncts.js (KeplerFuncts.getNumOrbits): Renamed function to KeplerFuncts.completedOrbits.
(KeplerFuncts.completedOrbits): returns completed orbits only (which is all it is capable of accurately doing 
without calculating arc length). Useful (in part) for calculating total distance when multiple orbits have been completed.

* scripts/KeplerOrbit.js (KeplerOrbit.getArcLength): Fixed bug with arctangent function (was using tangent!)
(KeplerOrbit.getArcLengthFromDateRange): Uses KeplerFuncts.completedOrbits()
(KeplerOrbit.addPartialOrbit): Added orbit years info. Made distance of one orbit its own info line
Added true anomaly 'extended' debug info (display can be disabled in KeplerSettings)
orbitEllipseResolution is now in KeplerSettings
Fixed 'orbit(s) completed' stats to be based off of distance, and not period-based time calculations (which are inaccurate
for partial orbits)

* scripts/KeplerSettings.js (constructor): Added showExtendedInfo (orbit debug info), 
circApproxFormula (which circumference approximation formula to use)

* scripts/KeplerSystem.js (KeplerSystem.drawPartialOrbits): Removed argument for orbitEllipseResolution (uses setting in KeplerSettings).



2014-07-28	Alex Koik-Cestone	<akoikcestone@gmail.com>

* scripts/EllipseLib.js (EllipseLib.arcLen): added debugging functionality

* scripts/KeplerFuncts.js (KeplerFuncts.conv): fixed bugs/made much faster
(KeplerFuncts.roundPreserve): New function. 'sig fix' preserving round function
(KeplerFuncts.getVList): New function. generate a number of true anomaly increments across the 2PI barrier.
More useful for calculating partial/complete orbits than previous method.
(KeplerFuncts.getNumOrbits): New function. Get # of orbits elapsed over a certain time.
(KeplerFuncts.getDays): New function. Get # of julian days elapsed between two time periods.
(KeplerFuncts.delimit): New function. Delimit numbers >= 1000, <= -1000 with commas

* scripts/KeplerSettings.js (constructor): Got rid of constant DEFAULT_ITERATIONS; replaced with orbitEllipseResolution.

* scripts/KeplerOrbit.js (KeplerOrbit.getArcLength): Now factors additional revolutions into the total arc length. Still bugged.
(KeplerOrbit.getArcLengthFromDateRange): Factors in additional rotations. Still bugged.
(KeplerOrbit.getOrbitVerticesCoords): Uses KeplerFuncts.getVList for easier/more robust calculations of orbit line increments
(KeplerOrbit.getPartialOrbit): Refactored orbit/day count into KeplerFuncts.getNumOrbits and KeplerFuncts.getDays
Added average speed calculations (Average speed and distance traveled are still bugged because of KeplerOrbit.getArcLength)



2014-07-24	Alex Koik-Cestone	<akoikcestone@gmail.com>

* index.html: Added 'date' panel.

* scripts/EllipseLib.js: New ellipse calculation library.
(EllipseLib.arcLen): Find the length of an elliptical arc.
(EllipseLib.circApprox): Find the circumference of an ellipse using various possible algorithms.

* scripts/KeplerFuncts.js (KeplerFuncts.getTimeSpan): option to return number or text string with unit.
(KeplerFuncts.addDrOrbitInfo): orbitInfo is now an object with properties.

* scripts/KeplerOrbit.js (Orbit.constructor): semi-minor axis and orbit circumference are now auto-filled element properties.
(Orbit.getArcLength): new function to get the length of an arc along the orbit. Has "2pi crossover" bug.
(Orbit.getArcLengthFromDateRange): new function that takes a date range and gets an arc length from it. uses getArcLength().
(Orbit.getOrbitVerticesCoords): has "2pi crossover" bug. Made note to fix.
(Orbit.addPartialOrbit): builds orbitInfo object to send to KeplerFuncts.addDROrbitInfo. Sends arc length info.

* scripts/KeplerSettings.js (constructor): changed some settings.

* scripts/KeplerSystems.js (System.constructor): added timers to allow 
1) finer control of selected times and 
2) repeated movement of objects through date range.
(Systems.addPartialsFromUser): HTML info panel timespan shows years, timers are initialized
(Systems.moveToPickedDate): move all systems to a picked date.
(Systems.removePickedDate): remove picked date, allowing systems to move forward in time again.
Still needs smooth transition (inc in ThreedFuncts.animate)

* scripts/KeplerUI.js: Add datepicker for single date.

* scripts/ThreedFuncts.js (ThreedFuncts.animate): improved functionality.
Fixed bug with simInc not responding to changes in KeplerSettings
Three modes: No date supplied, picked date, date range.
"No date" advances the date forward indefinitely.
Picked date keeps the date constant.
Date range continuously cycles through a range of dates with a start and end point.
simInc needs to be adjusted when picked date is removed

* styles/Kepler2-ui.css: add display:none to picked date removal button initial state