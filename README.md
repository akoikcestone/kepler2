# README #

* KeplerOrbits was originally a PHP script that allowed the user to see how far planets of the solar system had traveled in a given length of time.
* Kepler2 is a spiritual successor of KeplerOrbits which strives for much greater accuracy in its calculations and much better visualization. With Kepler2, the user can see planets move in realtime, pan the view of the solar system, and interact with planets on screen.
* Still in Alpha stage

### How do I get set up? ###

* Just download the repository and run index.html. Works with Firefox and Chrome at the moment.
* Configuration 
* Dependencies: THREE.js, jQuery
* How to run tests...
* Deployment instructions...

### Contribution guidelines ###

* Writing tests...
* Code review...
* Other guidelines...

### Who do I talk to? ###

* akoikcestone at gmail.com
* SirKoik on twitter