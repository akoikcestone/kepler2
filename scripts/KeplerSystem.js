function KeplerSystem(inName, color) {
	var _this = this;
	
	var systemName = 'system' + inName;
	this.systemName = 'system' + inName;
	this.systemFriendlyName = systems.systemFriendlyNames[systemName];
	
	
	this.system = new THREE.Object3D();
	this.system.name = systemName;
	
	var centerObj;
	var orbitingObjs = new Object();
	var orbitingObjsData;
	
	this.color = color;
	
	
	// Disabled dynamic loading of scripts. Scripts are all pre-loaded manually to avoid xss problems with file:///
	
	/*
	// load orbital system data
	// better to use JSON, using global helper variable for now to keep data files in readable/editable format
	this.xhrSystem = $.getScript('scripts/systems/'+inName+'.js')
	.done(function() {
		centerObj = SystemsData[inName].centerObj;
		orbitingObjsData = SystemsData[inName].orbitingObjs;
	})
	.fail(function(jqhxr, settings, exception) {
		KeplerFuncts.error('Failed to load system: ' + systemName + '<br/>Detailed error: ' + exception);
	});
	*/
	
	centerObj = SystemsData[inName].centerObj;
	orbitingObjsData = SystemsData[inName].orbitingObjs;
	
	
	// draw: draw the orbital system after data is loaded
	this.draw = function(callback) {
		$.when(this.xhrSystem).then(function() {
			_this.addCenterObj();			// add center object to system object
			_this.addOrbitingObjs();		// add orbiting objects to system object
			
			world.scene.add(_this.system);	//add system object to scene
			
			callback();
		});
	}
	
	
	// drawCenterObj: draw the center (orbited) object for a given orbiting object
	this.addCenterObj = function() {
		var obj = centerObj;
		var scaledRadius = Math.sqrt(obj.bulkParameters.rxy) * 6.68458712e-12 * KeplerSettings.objScaleFactor / 3;
		
		var centerGeometry = new THREE.SphereGeometry(scaledRadius, KeplerSettings.sphereResolution, KeplerSettings.sphereResolution);
		var centerMaterial = new THREE.MeshBasicMaterial({color: 0xffff00, overdraw: true});
		var centerMesh = new THREE.Mesh(centerGeometry, centerMaterial);
		
		if (KeplerSettings.showTextures) centerMaterial.map = new THREE.ImageUtils.loadTexture('scripts/models/' + obj.properties.texture);
		
		// make object 'upright'
		centerMesh.rotation.x = Math.PI / 2;
		
		// make object transparent so that very close orbits / other center objects are not blocked by it
		// temporary fix
		centerMesh.material.opacity = 0.75;
		centerMesh.material.overdraw = false;

		this.system.add(centerMesh);
	}
	
	
	// drawOrbitingObjs: draw orbiting objects and orbit lines
	this.addOrbitingObjs = function() {
		$.each(orbitingObjsData, function(objName, objData) {
			objData.systemName = systemName;
			orbitingObjs[objName] = new KeplerOrbit(objData);

			var orbit = orbitingObjs[objName];
			orbit.addOrbit();
		});
	}
	
	// drawPartialOrbits: draw partial orbits on demand
	// @scene: THREE.Scene object
	// @date0, @date1: Date objects
	this.drawPartialOrbits = function(scene, date0, date1, erasePrevOrbits) {
		//alert(Object.keys(orbitingObjs).length);
		$.each(orbitingObjs, function(objName, orbit) {
			orbit.addPartialOrbit(scene, date0, date1, erasePrevOrbits);
		});
	}
	
	
	// addPartialsFromUser: add partial orbits from user input
	this.addPartialsFromUser = function(date0, date1) {	
		this.drawPartialOrbits(world.scene, date0, date1, true);
		return false;
	}
	
	
	// removePartials: remove all partial orbits
	this.removePartials = function() {
		$.each(orbitingObjs, function(orbitName, orbit) {
			orbit.removePartialOrbits();
		});
	}
	
	
	// update: update positions/rotations of objects
	this.update = function(newDate) {
		$.each(orbitingObjs, function(k, v) {
			//var newMA = KeplerFuncts.getMeanAnomaly(newDate, this.el.M0, this.el.P);
			//this.el.M = newMA;
			//this.el.E = this.getEccAnomaly(this.el.e, this.el.M);
			
			//alert('OrbitingObjs update: ' + this.el.name);
			
			var el = this.el;
			var newV = this.getVfromDate(newDate);
			el.V = newV;
			
			var newCoords = this.getPosR(el);
			
			var rotInc = 2 * Math.PI * KeplerSettings.simInc / this.el.bulkParameters.p;
			
			this.updObject(newCoords, rotInc, k);
		});
	}
}