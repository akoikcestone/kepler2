var ThreedFuncts = new function() {
	var _this = this;
	
	
	// draw axis indicators
	this.drawAxisIndicators = function(scene) {
		scene.add(this.drawLine([0, 0, 0], [20, 0, 0], 0xFF0000));
		scene.add(this.drawArrowhead([20, 0, 0], 0xFF0000, 'x'));
		scene.add(this.drawLine([0, 0, 0], [0, 20, 0], 0x00FF00));
		scene.add(this.drawArrowhead([0, 20, 0], 0x00FF00, 'y'));
		scene.add(this.drawLine([0, 0, 0], [0, 0, 20], 0x0000FF));
		scene.add(this.drawArrowhead([0, 0, 20], 0x0000FF, 'z'));
	}
	
	
	// draw 2-point lines
	// drawLine: draw simple straight line
	this.drawLine = function(from, to, color) {
		if (typeof color === 'undefined') color = 0x0000FF;
		var material = new THREE.LineBasicMaterial({color: color});
		var geometry = new THREE.Geometry();
		geometry.vertices.push(new THREE.Vector3(from[0], from[1], from[2]));
		geometry.vertices.push(new THREE.Vector3(to[0], to[1], to[2]));
		
		var line = new THREE.Line(geometry, material);
		return line;
	}
	
	
	// draw arrowheads
	this.drawArrowhead = function(pos, color, axis) {
		if (typeof color === 'undefined') color = 0x0000FF;
		var geometry = new THREE.CylinderGeometry(0, 1, 5, 16);
		var material = new THREE.MeshBasicMaterial({color: color});
		var cone = new THREE.Mesh(geometry, material);
		
		cone.position.x = pos[0];
		cone.position.y = pos[1];
		cone.position.z = pos[2];
		
		if (axis == 'x') cone.rotation.z = -Math.PI / 2;
		//if (axis == 'y') cone.rotation.z = -Math.PI * 3 / 2;
		if (axis == 'z') cone.rotation.x = Math.PI / 2;
		
		return cone;
	}
	
	
	// draw cube (box)
	this.drawBox = function(pos, width, height, depth, color) {
		var geometry = new THREE.BoxGeometry(width, height, depth, 2, 2, 2);
		var material = new THREE.MeshBasicMaterial({color:  color});
		var box = new THREE.Mesh(geometry, material);
		
		box.position.x = pos[0];
		box.position.y = pos[1];
		box.position.z = pos[2];
		
		//box.rotation.y = Math.PI;
		//box.rotation.x = Math.PI;
		
		return box;
	}
	
	// draw text
	this.drawText = function(text, font, size, pos, color) {
		var geometry = new THREE.TextGeometry(text, {font: font, size: size, height: 0.1});
		var material = new THREE.MeshBasicMaterial({color:  color});
		var text = new THREE.Mesh(geometry, material);
		
		text.position.x = pos[0];
		text.position.y = pos[1];
		text.position.z = pos[2];
		
		//box.rotation.y = Math.PI;
		text.rotation.x = Math.PI / 2;
		
		return text;
	}	

	
	// drawBoxLines: extrude a thick 'line' along a path made of cylinders
	// courtesy WestLangley
	this.drawBoxLines = function(vertices, width, color) {
		var vertex, geometry, material, mesh;
		var segments = new THREE.Object3D();
		
		var textVerts = '';
		$.each(vertices, function(k,v) {
		
			textVerts+= '\n'+'points.push(['+v.x+','+v.y+','+v.z+']);';
			//alert(v.getComponent[0] + ',' + v.getComponent[1] + ',' + v.getComponent[2]);
		});
		
		//alert(textVerts);
		
		/*
		var vertices = [];
		for (var x = 0; x < verticesIn.length; x++) {
			var vertex = verticesIn[x];
			vertices.push(new THREE.Vector3(vertex[0], vertex[1], vertex[2]));
		}
		*/
		
		width = width / 10;
		
		for (var i = 0, len = vertices.length - 1; i < len; i++) {
			vertex = vertices[i];
			//alert(vertex.toString());
			geometry = new THREE.CylinderGeometry(1, 1, 1, 4, 1, false);
			geometry.applyMatrix( new THREE.Matrix4().makeRotationX( Math.PI / 2 ) );
			material = new THREE.MeshBasicMaterial({color: color, overdraw: true});
				
				/*new THREE.MeshNormalMaterial({
				opacity: 0.5,
				transparent: true
			});*/

			mesh = new THREE.Mesh(geometry, material);
			mesh.position = vertex;
			mesh.lookAt(vertices[1 + i]);

			length = vertex.distanceTo(vertices[1 + i]);
			mesh.scale.set(width, width, length + width);
			mesh.translateZ(0.5 * length);
			segments.add(mesh);
		}
		return segments;
	}
	this.convert = function(vertex) {
		return new THREE.Vector3(vertex[0], vertex[1], vertex[2]);
	}
	
	
	// draw 2d orbit fill.
	// draw between [x, y, z] = 0 and vertices along orbit
	this.drawOrbitFill = function(coords, color) {
		var geometry = new THREE.Geometry();
		var material = new THREE.MeshBasicMaterial({color: color, side: THREE.DoubleSide});
		
		geometry.vertices.push(new THREE.Vector3(0, 0, 0));
		geometry.vertices.push(new THREE.Vector3(-10, 0, 10));
		geometry.vertices.push(new THREE.Vector3(0, 0, 20));
		geometry.vertices.push(new THREE.Vector3(10, 0, 10));
		
		geometry.faces.push(new THREE.Face3(0, 1, 2));
		geometry.faces.push(new THREE.Face3(0, 2, 3));
		geometry.computeBoundingSphere();
		
		var orbitFill = new THREE.Mesh(geometry, material);
		return orbitFill;
	}
	
	
	this.canvasCursor = function(arg) {
	//alert($('#'+kepler2CanvasID).css('cursor'));
		var cursorCss = arg == 'on'? 'pointer' : 'auto';
		$('#' + kepler2CanvasID).css('cursor', cursorCss);
	}
	
	
	var simInc = KeplerSettings.simInc * 86400000; // simulation increment in earth days
	// this.updSimInc: update increment in the simulation speed;
	this.updSimInc = function(incValue) {
		//inc = 1;
		simInc = incValue * 86400000;
	}
	

	
	var t0 = new Date().getTime();
	//var timeSum = 0;
	// this.animate: animate motion of the planets indefinitely
	this.animate = function() {
		// stay on fixed 'picked date' if selected
		if (systems.pickedDate != null) {
			newDate = systems.pickedDate;
			t0 = newDate.getTime();
			KeplerFuncts.addInfo(newDate.toString());
			systems.update(newDate);
			return false;
		}
		
		if (systems.lastFromDate != null && systems.lastToDate != null) {
			if (t0 < systems.fromTime || t0 + simInc > systems.toTime) t0 = systems.fromTime;
		}
		
		t0 += simInc;
		//timeSum += simInc;
		
		var newDate = new Date(t0);
		
		// update all active systems
		systems.update(newDate);
		KeplerFuncts.addInfo(newDate.toString());
	}
	
	
	// this.chgView: change view to front, top, or right side of system with tweening
	// uses modified OrbitControls whose spherical phi, th angles can be programmatically modified
	// @dir String direction to change to
	
	this.chgView = function(dir) {
		var cam = world.camera;
		camPos = cam.position;

		var wcPhi = world.controls.phi;
		var wcTh = world.controls.theta;
		
		switch(dir) {
			case 't':		
				upRot = 0;
				leftRot = 0;
				break;
			case 'f':
				// change to inclined view 'front' if enabled in settings
				upRot = KeplerSettings.inclinedView? Math.PI / 2 - KeplerSettings.inclinedViewIncline : Math.PI / 2;
				leftRot = 0;
				break;
			case 'r':
				upRot = Math.PI / 2;
				leftRot = Math.PI / 2;
				break;
		}

		// get difference between desired phi/theta and current
		var phiDiff = Math.abs(wcPhi - upRot);
		var thDiff = Math.abs(wcTh - leftRot);
		
		// tFrac ensures constant rotation speed (i.e. a full rotation takes 2 sec, 
		// half of a rotation takes 1 sec). speed dictated by biggest angle
		var tFrac = phiDiff > thDiff? phiDiff / Math.PI : thDiff / Math.PI;
		
		t = 2000 * tFrac;
		
		var rotStart = {up: world.controls.phi, left: world.controls.theta};
		var rotEnd   = {up: upRot, left: leftRot};

		world.controls.updateMode = 'programmatic';
		
		// TODO stop rot tween if new rot tween called
		/*if (rotTween instanceof TWEEN.Tween) {
			rotTween.paused = true; 
			rotTween = null;
			//rotTween.removeAllTweens();
			return false;
		}*/
		
		var rotTween = new TWEEN.Tween(rotStart).to(rotEnd, t);
		
		rotTween.onUpdate(function() {
			world.controls.theta = rotStart.left;
			world.controls.phi = rotStart.up;
		});
		
		rotTween.onComplete(function() {
			// enabling recursive chgView results in blank screen?
			// now chgView doesn't need to be recursive to work?
		
			// chgView recursively calls until camera lines up with axes.
			// hack to get around fact that tweening OrbitControls theta/phi values directly
			// does not result in clean lineup with axes in one chgView call.
			
			if (phiDiff > 0 || thDiff > 0) {
				//_this.chgView(dir);
			} else {
				//world.controls.updateMode = 'user';
				//alert(world.camera.position.x + '\n' + camPos.y + '\n' + camPos.z);
			}
		});
		
		rotTween.start();
		
	}
	
	
	// this.zoom: tweened zoom between two increments
	// uses OrbitControls
	//
	// @dir String zoom 'in' or 'out'
	// @inc0 Number starting zoom increment
	// @inc1 Number ending zoom increment
	this.zoom = function(dir, inc0, inc1) {
		var zoomTween = new TWEEN.Tween(inc0).to(inc1, 200);
		zoomTween.onUpdate(function() {
			if (dir == 'in') world.controls.dollyIn(inc0)
			if (dir == 'out') world.controls.dollyOut(inc0);
		});
		
		zoomTween.start();
		
		zoomTween.onComplete(function() {
			var camPos = world.camera.position;
			world.camRadius = Math.sqrt(Math.pow(camPos.x, 2) + Math.pow(camPos.y, 2) + Math.pow(camPos.z, 2));
		});
	}
	
	this.orbitRotate = function(dir, inc0, inc1) {
		//var 
	}
	
	
	// toggleAnimation: toggle the animation of the world
	var animating = true;
	this.toggleAnimation = function() {
		if (KeplerSettings.animate) {
			KeplerSettings.animate = false;
			$('#start-stop').val('continue');
		} else {
			KeplerSettings.animate = true;
			$('#start-stop').val('pause');
		}
	}
	
	
	// toggleAnimationLoop: toggle animation loop
	this.toggleAnimationLoop = function() {
		if (animating) {
			KeplerSettings.animate = false;
			cancelAnimationFrame(animFrame);
			animating = false;
			$('#toggle-anim-loop').val('restart loop');
		} else {
			KeplerSettings.animate = true;
			animating = true;
			$('#toggle-anim-loop').val('pause loop');
			//render();
			anim();
		}		
	}
	
	
	// update on resize
	window.onresize = function() {
		world.camera.aspect = window.innerWidth / window.innerHeight;
		world.camera.updateProjectionMatrix();
		world.renderer.setSize(window.innerWidth, window.innerHeight);
	}
}