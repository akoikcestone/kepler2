SystemsData.test = {
centerObj: {
	name: 'Sol',
	bulkParameters: {
		rxy: 695800,
		rz: 695800 * (1 - 9e-6),
		M: 1.98855e30,
		P: 25.05,
		at: 0,
		L: 1
	},
	properties: {
		objType: 'ellipsoid',
		texture: 'Sun.jpg',
		credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
	}
},

orbitingObjs: {
	'test object': {
		name: 'Test',
		a: 2,
		e: 0.5,
		P: 800,
		M0: 0 * toRad,
		i: 20 * toRad,
		O: 90 * toRad, //O: 45 * toRad,
		w: 45 * toRad, //w: 20 * toRad,
		bulkParameters: {
			rxy: 1000,
			rz: 900,
			M: 1.8986e27,
			p: 9.925 / 24,
			at: 20 * toRad,
			A: 0.343
		}
	}
	/*
	'example object': {
		name: 'Example satellite',
		a: 26.56046,
		e: 0.0127851,
		P: 20000,
		M0: 322.378 * toRad,
		i: 56.26 * toRad,
		O: 342.08 * toRad, //O: 45 * toRad,
		w: 179.53 * toRad, //w: 20 * toRad,
		bulkParameters: {
			rxy: 10,
			rz: 10,
			M: 1.8986e27,
			p: 9.925 / 24,
			at: 3.03 * toRad,
			A: 0.343
		}
	}
	*/
}
}