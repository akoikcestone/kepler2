SystemsData.test2 = {
centerObj: {
	name: 'Sol',
	bulkParameters: {
		rxy: 695800,
		rz: 695800 * (1 - 9e-6),
		M: 1.98855e30,
		P: 25.05,
		at: 0,
		L: 1
	},
	properties: {
		objType: 'ellipsoid',
		texture: 'Sun.jpg',
		credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
	}
},

orbitingObjs: {
	'jupiter': {
		name: 'Jupiter',
		a: 5.20336301,
		e: 0.04839266,
		P: 4332.59,
		M0: 18.818 * toRad,
		i: 1.30530 * toRad,
		O: 100.55615 * toRad,
		w: 14.75385 * toRad,
		bulkParameters: {
			rxy: 71492,
			rz: 66854,
			M: 1.8986e27,
			p: 9.925 / 24,
			at: 3.03 * toRad,
			A: 0.343
		}
	},
	'pluto': {
		name: 'Pluto',
		a: 39.48168677, 
		e: 0.24880766,
		P: 90465, 
		M0: 14.86012204 * toRad,
		O: 110.30347 * toRad,
		w: 224.06676 * toRad,
		i: 17.14175 * toRad,
		bulkParameters: {
			rxy: 1184,
			rz: 1184,
			M: 1.305e22,
			p: 6.387230,
			at: 119.591 * toRad,
			A: (0.49 + 0.66) / 2
		}
	}
}

}