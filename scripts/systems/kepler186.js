SystemsData['kepler186'] = {
centerObj: {
	name: 'Kepler-186',
	bulkParameters: {
		rxy: 0.47 * rSol,
		rz: 0.47 * rSol,
		M: 0.48 * mSol,
		P: 0,						// unknown
		at: 0,						// unknown
		L: 0.0412,
		T: 3788
	},
	properties: {
		objType: 'ellipsoid',
		texture: 'Sun.jpg',
		credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
	}
},

orbitingObjs: {
	'b': {
		name: 'b',
		a: 0.0378,
		e: 0.24,
		P: 3.8867907,
		M0: 0 * toRad,				// unknown
		i: 83.65 * toRad,
		O: 0 * toRad,				// unknown
		w: 0 * toRad, 				// unknown
		bulkParameters: {
			rxy: 1.08 * rEarth,
			rz: 1.08 * rEarth,
			M: 0,					// unknown
			p: 3.8867907, 			// most likely tidally locked, P = p
			at: 0 * toRad,			// unknown
			A: 0.2					// unknown
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Mercury.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	},

	'c': {
		name: 'c',
		a: 0.0574,
		e: 0.24,
		P: 7.267302,
		M0: 0 * toRad,				// unknown
		i: 90 - 85.94 * toRad,
		O: 0 * toRad,				// unknown
		w: 0 * toRad, 				// unknown
		bulkParameters: {
			rxy: 1.25 * rEarth,
			rz: 1.25 * rEarth,
			M: 0,					// unknown
			p: 7.267302, 			// most likely tidally locked, P = p
			at: 0 * toRad,			// unknown
			A: 0.2					// unknown
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Mercury.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	},
	
	'd': {
		name: 'd',
		a: 0.0861,
		e: 0.25,
		P: 13.342996,
		M0: 0 * toRad,				// unknown
		i: 90 - 87.09 * toRad,
		O: 0 * toRad,				// unknown
		w: 0 * toRad, 				// unknown
		bulkParameters: {
			rxy: 1.39 * rEarth,
			rz: 1.39 * rEarth,
			M: 0,					// unknown
			p: 13.342996, 			// most likely tidally locked, P = p
			at: 0 * toRad,			// unknown
			A: 0.2					// unknown
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Mercury.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	},

	'e': {
		name: 'e',
		a: 0.1216,
		e: 0.24,
		P: 22.407704,
		M0: 0 * toRad,				// unknown
		i: 90 - 88.24 * toRad,
		O: 0 * toRad,				// unknown
		w: 0 * toRad, 				// unknown
		bulkParameters: {
			rxy: 1.33 * rEarth,
			rz: 1.33 * rEarth,
			M: 0,					// unknown
			p: 22.407704, 			// most likely tidally locked, P = p
			at: 0 * toRad,			// unknown
			A: 0.2					// unknown
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Mercury.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	},
	
	'f': {
		name: 'f',
		a: 0.3926,
		e: 0.34,
		P: 129.9459,
		M0: 0 * toRad,				// unknown
		i: 90 - 89.9 * toRad,
		O: 0 * toRad,				// unknown
		w: 0 * toRad, 				// unknown
		bulkParameters: {
			rxy: 1.11 * rEarth,
			rz: 1.11 * rEarth,
			M: 0,					// unknown
			p: 129.9459, 			// possibly tidally locked, P = p
			at: 0 * toRad,			// unknown
			A: 0.2					// unknown
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Mercury.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	}
}
}