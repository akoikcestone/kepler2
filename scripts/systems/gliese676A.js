// M0 / Tperi still neeeds to be implemented for "b"

SystemsData['gliese676A'] = {
centerObj: {
	name: 'Gliese 676A',
	bulkParameters: {
		rxy: 446520, 				// R = sqrt[(0.082*3.846e26)/pi/4/(5.67e-8)/(3860^4)]
		rz: 446520,
		M: 0.71 * 1.98855e30,
		P: 0,						// unknown
		at: 0,						// unknown
		L: 0.082,
		ld: 54,
		T: 4000						// unknown
	},
	properties: {
		objType: 'ellipsoid',
		texture: 'Sun.jpg',
		credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
	}
},

orbitingObjs: {
	'd': {
		name: 'd',
		a: 0.0413,
		e: 0.15,
		P: 3.6000,
		M0: 0 * toRad,				// unknown
		i: 0 * toRad,				// unknown
		O: 0 * toRad,				// unknown
		w: 315.0 * toRad, 			// error +/- 109.0 deg!
		bulkParameters: {
			rxy: 10434, 			// assuming density = earth and V proportionate to M
			rz: 10434,
			M: 5.97219e24 * 4.4,
			p: 3.6000, 				// most likely tidally locked, P = p
			at: 0 * toRad,			// unknown
			A: 0.2					// unknown
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Mercury.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	},
	
	'e': {
		name: 'e',
		a: 0.187,
		e: 0.24,
		P: 35.37,
		M0: 0 * toRad,				// unknown
		i: 0 * toRad,				// unknown
		O: 0 * toRad,				// unknown
		w: 332.0 * toRad, 			// error +/- 126.0 deg!
		bulkParameters: {
			rxy: 21927, 			// assuming density = neptune and V proportionate to M
			rz: 21927,
			M: 5.97219e24 * 11.5, 	// = 0.6705x neptune
			p: 35.37, 				// most likely tidally locked, P = p
			at: 0 * toRad,			// unknown
			A: 0.2					// unknown
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Neptune.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	},
	
	'b': {
		name: 'b',
		a: 1.8,
		e: 0.328,
		P: 1050.3,
		M0: 0 * toRad,				// need to calculate
		TPeri: 2455411.9, 			// 3866.9 days after J2000
		i: 0 * toRad,				// unknown
		O: 0 * toRad,				// unknown
		w: 86.9 * toRad,
		bulkParameters: {
			rxy: 70000,				// unknown. guess based on fact that most planets heavier than Jupiter are not much larger
			rz: 70000,
			M: 1.8986e27 * 4.95,
			p: 1,					// unknown
			at: 0,					// unknown
			A: 0.2					// unknown
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Jupiter.jpg',
			credit: 'Cassini mission'
		}
	},

	'c': {
		name: 'c',
		a: 5.2,
		e: 0.2,
		P: 4400,
		M0: 0 * toRad,				// unknown
		i: 0 * toRad,				// unknown
		O: 0 * toRad,				// unknown
		w: 356.0 * toRad,
		bulkParameters: {
			rxy: 70000,				// unknown. guess based on fact that most planets heavier than Jupiter are not much larger
			rz: 70000,
			M: 1.8986e27 * 3,
			p: 1,					// unknown
			at: 0,					// unknown
			A: 0.2					// unknown
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Jupiter.jpg',
			credit: 'Cassini mission'
		}
	}
}

}