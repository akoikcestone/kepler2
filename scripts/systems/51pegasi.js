SystemsData['51pegasi'] = {
centerObj: {
	name: '51 Pegasi',
	bulkParameters: {
		rxy: 1.237 * 695800,
		rz: 1.237 * 695800,
		M: 1.11 * 1.98855e30,
		P: 21.9,
		at: 0,
		L: 1.30,
		ld: 50.9,
		T: 5571
	},
	properties: {
		objType: 'ellipsoid',
		texture: 'Sun.jpg',
		credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
	}
},

orbitingObjs: {
	'b': {
		name: 'b',
		a: 0.0527,
		e: 0,
		P: 4.23077,
		M0: 0 * toRad,
		i: 0 * toRad,
		O: 0 * toRad,
		w: 0 * toRad,
		bulkParameters: {
			rxy: 72000,
			rz: 72000,
			M: 1.8986e27 * 0.468,
			p: 58.646,
			at: 0 * toRad,
			A: 0.1
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Jupiter.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	}
}

}