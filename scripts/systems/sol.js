SystemsData.sol = {
centerObj: {
	name: 'Sol',
	bulkParameters: {
		rxy: 695800,
		rz: 695800 * (1 - 9e-6),
		M: 1.98855e30,
		P: 25.05,
		at: 0,
		L: 1,
		T: 5778,
		FE: 1367
	},
	properties: {
		objType: 'ellipsoid',
		texture: 'Sun.jpg',
		credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
	}
},

orbitingObjs: {
	'mercury': {
		name: 'Mercury',
		a: 0.387098,
		e: 0.205630,
		P: 87.969,
		M0: 174.796 * toRad,
		i: 7.005 * toRad,
		O: 48.331 * toRad,
		w: 29.124 * toRad,
		bulkParameters: {
			rxy: 2439.7,
			rz: 2439.7,
			M: 3.3022e23,
			p: 58.646,
			at: (2.11/60) * toRad,
			A: 0.068,
			nightTemp: 100,
			gh: -111.76
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Mercury.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	},
	
	'venus': {
		name: 'Venus',
		a: 0.723327,
		e: 0.0067,
		P: 224.701,
		M0: 50.115 * toRad,
		i: 3.39458 * toRad,
		O: 76.678 * toRad,
		w: 55.186 * toRad,
		bulkParameters: {
			rxy: 6051.8,
			rz: 6051.8,
			M: 4.8676e24,
			p: 243.0185,
			at: 177.36 * toRad,
			A: 0.90,
			gh: 546.85
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Venus.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	},
	
	'earth': {
		name: 'Earth',
		a: 1.00000261,
		e: 0.01671123,
		P: 365.256363004,
		M0: 357.51716 * toRad,
		i: 0 * toRad,
		O: 348.73936 * toRad,
		w: 114.20783 * toRad,
		bulkParameters: {
			rxy: 6378.1,
			rz: 6356.8,
			M: 5.97219e24,
			p: 0.99726968,
			at: (23 + 26/60 + 21.4119 / 3600) * toRad,
			A: 0.306,
			gh: 26.05
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Earth.jpg',
			credit: 'http://www.johnstonsarchive.net/spaceart/cylmaps.html'
		},
		description: 'The verdant homeworld of the human race.'
	},

	'mars': {
		name: 'Mars',
		a: 1.523679,
		e: 0.093315,
		P: 686.971,
		M0: 19.3564 * toRad,
		i: 1.850 * toRad,
		O: 49.562 * toRad,
		w: 286.537 * toRad,
		bulkParameters: {
			rxy: 3396.2,
			rz: 3376.2,
			M: 6.4185e23,
			p: 1.025957,
			at: 25.19 * toRad,
			A: 0.25,
			gh: -6.52
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Mars.jpg',
			credit: 'classe.cornell.edu/~seb/celestia/gallery-002.html'
		}
	},

	'vesta': {
		name: 'Vesta',
		a: 2.362,
		e: 0.08862,
		P: 1325.85,
		M0: 307.80 * toRad,
		i: 7.134 * toRad,
		O: 103.91 * toRad,
		w: 149.84 * toRad,
		bulkParameters: {
			rxy: (572.6 + 557.4) / 2,
			rz: 446.4,
			M: 2.59076e20,
			p: 0.2226,
			at: 27 * toRad,
			A: 0.423,
			gh: 14.43
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Mercury.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	},
	
	
	'ceres': {
		name: 'Ceres',
		a: 2.7668,
		e: 0.075797,
		P: 1680.99,
		M0: 10.557 * toRad,
		i: 10.593 * toRad,
		O: 80.3276 * toRad,
		w: 72.2921 * toRad,
		bulkParameters: {
			rxy: 487.3,
			rz: 454.7,
			M: 9.43e20,
			p: 0.3781,
			at: 3 * toRad,
			A: 0.090,
			gh: 0
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Mercury.jpg',
			credit: 'http://www.solarsystemscope.com/nexus/textures/planet_textures/'
		}
	},	
	
	'jupiter': {
		name: 'Jupiter',
		a: 5.20336301,
		e: 0.04839266,
		P: 4332.59,
		M0: 18.818 * toRad,
		i: 1.30530 * toRad,
		O: 100.55615 * toRad,
		w: 14.75385 * toRad,
		bulkParameters: {
			rxy: 71492,
			rz: 66854,
			M: 1.8986e27,
			p: 9.925 / 24,
			at: 3.03 * toRad,
			A: 0.343,
			gh: 51.1
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Jupiter.jpg',
			credit: 'Cassini mission'
		},
		description: 'An enormous world of compressed hydrogen and helium gas cloaked in windy cloud belts which give it a distinct banded appearance. Violent storms larger than Earth dot the surface, and a radiation belt fatal to man and machine alike extends past the orbits of the moons Io and Europa.'
	},

	'saturn': {
		name: 'Saturn',
		a: 9.53707032,
		e: 0.05415060,
		P: 10759.22,
		M0: 320.346750 * toRad,
		i: 2.48446 * toRad,
		O: 113.71504 * toRad,
		w: 92.43194 * toRad,
		bulkParameters: {
			rxy: 60268,
			rz: 54364,
			M: 5.6846e26,
			p: 10.57 / 24,
			at: 26.73 * toRad,
			A: 0.342,
			gh: 49.72
		},
		properties: {
			//objType: 'custom',
			//path: 'Saturn/Saturn.js',
			objType: 'ellipsoid',
			texture: 'Saturn.jpg',
			credit: 'astro.uni-altai.ru'
		},
		description: 'This gigantic buttercream world is host to the most extensive planetary rings in the solar system.'
	},

	'uranus': {
		name: 'Uranus',
		a: 19.19126393,
		e: 0.04716771,
		P: 30685.4,
		M0: 142.955717 * toRad,
		i: 0.76986 * toRad,
		O: 74.22988 * toRad,
		w: 170.96424 * toRad,
		bulkParameters: {
			rxy: 25559,
			rz: 24973,
			M: 8.6810e25,
			p: 0.71833,
			at: 97.77 * toRad,
			A: 0.300,
			gh: 15.22
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Uranus.jpg',
			credit: 'astro.uni-altai.ru'
		}	
	},

	'neptune': {
		name: 'Neptune',
		a: 30.06896348,
		e: 0.00858587,
		P: 60189,
		M0: 267.767281 * toRad,
		i: 1.76917 * toRad,
		O: 131.72169 * toRad,
		w: 44.97135 * toRad,
		bulkParameters: {
			rxy: 25764,
			rz: 24341,
			M: 1.0243e26,
			p: 0.6713,
			at: 28.32 * toRad,
			A: 0.290,
			gh: 23.26
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Neptune.jpg',
			credit: 'textures.forrest.cz'
		}
	},

	'pluto': {
		name: 'Pluto',
		a: 39.48168677, 
		e: 0.24880766,
		P: 90465, 
		M0: 14.86012204 * toRad,
		O: 110.30347 * toRad,
		w: 224.06676 * toRad,
		i: 17.14175 * toRad,
		bulkParameters: {
			rxy: 1184,
			rz: 1184,
			M: 1.305e22,
			p: 6.387230,
			at: 119.591 * toRad,
			A: (0.49 + 0.66) / 2,
			gh: 6.39
		},
		properties: {
			objType: 'ellipsoid',
			texture: 'Pluto.png',
			credit: 'boulder.swri.edu/~buie/pluto/maptoys.html'
		}
	}
}

}