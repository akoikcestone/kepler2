function KeplerSystems() {
	var _this = this;
	this.systems = new Object();
	this.systemIds = [];
	this.systemFriendlyNames = new Object();
	this.loadedSystems = new Object();
	
	var colorIndex = 0;
	
	this.lastFromDate = null;
	this.lastToDate = null;
	
	this.timerStart = null; // start time for timer
	this.fromTime = null;
	this.toTime = null;
	
	this.pickedDate = null; // set date to stay at
	
	this.init = function() {
		// disabled json list of systems
		// using js list of systems
		/*
		var listXhr = $.getJSON('scripts/systems/systems.json', function(data) {
			_this.systems = data.systems;
		})
		.fail(function() {
			KeplerFuncts.error('Failed to load list of systems.');
		});*/
		
		_this.systems = systemsList;
		
		//$.when(listXhr).then(function() {			
			// load default systems
			$.each(KeplerSettings.defaultSystems, function(i, system) {
				_this.loadSystem(system);
			});
			
			// load system list (incl. selected systems)
			_this.loadSystemList();
			
			//render();
		//});
		
		
		// list selector code
		/*
		$(function() {
			$('.list-selector').on('click', function() {
				var id = $(this).attr('id');
				alert(id);
			});
		});*/
	}
	
	
	// loadSystemList: load list of available systems
	// select choices loaded by default
	this.loadSystemList = function() {
		$.each(this.systems, function(i, system) {
			var systemName = system.name;
			var systemId = system.id;
		
			var systemLoaded = (typeof _this.loadedSystems['system'+systemId] !== 'undefined');
			var sel = systemLoaded? ' selected' : '';
			$('#system-selector').append('<div class="list-selector' + sel + '" id="system-sel-' + systemId + '"><span class="check' + sel + '">&bull;</span> ' + systemName + '</div>');
			
			//$('#system-list').append('<li class="systemName">'+systemName);
			_this.systemIds.push(systemId);
			_this.systemFriendlyNames['system'+systemId] = systemName;
		});

		// bind list selector change code
		$('.list-selector').on('click', null, function() {
			var id = $(this).attr('id').substring(11);
			var isSelected = $(this).hasClass('selected');
			//alert(id + '\n' + isSelected);
			
			//if (isSelected && _this.loadedSystems.length < 2) return false; // cannot select fewer than 1 system
			//if (!isSelected) _this.loadSystem(id);
			_this.toggleSystem(id);
			
			$(this).toggleClass('selected');
			$(this).children('.check').toggleClass('selected');
		});
	}
	
	
	// loadSystem: load a system
	this.loadSystem = function(systemName) {
		var color = KeplerSettings.orbit.colors[colorIndex];
		colorIndex = colorIndex < KeplerSettings.orbit.colors.length - 1? colorIndex + 1 : 0;
		
		var system = new KeplerSystem(systemName, color);
		var properName = 'system'+systemName;
		
		this.loadedSystems[properName] = system;
		this.loadedSystems[properName].draw(function() {
			// add any partial orbits
			//if (this.lastFromDate != null) alert(this.lastFromDate.toString());
			if (_this.lastFromDate != null && _this.lastToDate != null) {
				_this.loadedSystems[properName].addPartialsFromUser(_this.lastFromDate, _this.lastToDate);
			}
		});
	}
	
	
	// toggleSystem: show/hide a system
	this.toggleSystem = function(systemId) {
		var systemProperId = 'system' + systemId;
		var systemObj = world.scene.getObjectByName(systemProperId);
		
		if (typeof systemObj === 'undefined') {
			_this.loadSystem(systemId);
		} else {
			systemObj.traverse(function(obj) {
				obj.visible = obj.visible? false : true;
			});
		}
	}
	
	// addPartialsFromUser: add partial orbits for each system
	this.addPartialsFromUser = function(date0Val, date1Val) {
		var date0Val = $('#date-from-div').datepicker('getDate');
		var date1Val = $('#date-to-div').datepicker('getDate');
		
		if (date0Val == '' || date1Val == '') {
			KeplerFuncts.error('Invalid date range');
			return false;
		} else {
			KeplerFuncts.clearError('Invalid date range');
		}
	
		var date0 = new Date(date0Val);
		var date1 = new Date(date1Val);	
		
		// check if date range was already added
		if (date0 == this.lastFromDate && date1 == this.lastToDate) {
			KeplerFuncts.userError('Date range ' + date0.toString() + ' - ' + date1.toString() + ' was already added.');
			return false;
		}
		
		this.lastFromDate = date0;
		this.lastToDate = date1;
		
		// clear date range orbit info before adding
		KeplerFuncts.clearDROrbitInfo();
		
		$.each(systems.loadedSystems, function(i, system) {
			system.addPartialsFromUser(date0, date1);
		});
		
		$('#removePartials-button').fadeIn();
		
		// fill in date range info panel
		$('#dr-start').html(date0.toString().replace(/\sGMT.*/, ''));
		$('#dr-stop').html(date1.toString().replace(/\sGMT.*/, ''));
		
		var rangeTimespan = KeplerFuncts.getTimespan(date0, date1, false);
		$('#dr-timespan').html(KeplerFuncts.delimit(rangeTimespan) + ' days (' + KeplerFuncts.delimit(KeplerFuncts.round(rangeTimespan / 365.25, 2)) + ' years)');
		$('#dr-rel-start').html(KeplerFuncts.delimit(KeplerFuncts.getTimespan(date0, new Date(), false)) + ' days ago');
		$('#dr-rel-stop').html(KeplerFuncts.delimit(KeplerFuncts.getTimespan(date1, new Date(), false)) + ' days ago');
		$('.dr-infopanel').show();
		
		// set start timer and from/to time positions
		this.timerStart = new Date().getTime();
		this.fromTime = this.lastFromDate.getTime();
		this.toTime = this.lastToDate.getTime();
	}
	
	
	// removePartials: remove partial orbits for each system
	this.removePartials = function() {
		this.lastFromDate = null;
		this.lastToDate = null;
	
		$.each(systems.loadedSystems, function(i, system) {
			system.removePartials();
		});
		
		$('#removePartials-button').fadeOut();
		
		// hide date range info panel
		$('.dr-infopanel').hide();
	}
	
	
	// moveToPickedDate: move systems to date set by date picker
	this.moveToPickedDate = function() {
		var pickedDate = new Date($('#date-div').datepicker('getDate'));
		KeplerFuncts.addInfo(pickedDate.toString());
		//alert(pickedDate.toString());
		
		this.pickedDate = pickedDate;
		if ($('#removePickedDate-button').is(':hidden')) $('#removePickedDate-button').fadeIn();
	}
	
	// removePickedDate: remove the picked date
	this.removePickedDate = function() {
		this.timerStart = this.pickedDate.getTime(); // start planets at this time
	
		this.pickedDate = null;
		$('#removePickedDate-button').fadeOut();
		$('#date-div').datepicker('setDate', new Date());
	}


	// this.getSysData: return system data for a given system
	// @systemName String name of system
	// @return Object system data
	this.getSysData = function(systemName) {
		return SystemsData[systemName];
	}
	
	
	// this.update: update all systems based on a given date
	this.update = function(newDate) {
		$.each(systems.loadedSystems, function(i, system) {
			system.update(newDate);
		});
	}
	
	
	// this.planetInfoBox: popup showing selected planet information
	this.planetInfoBox = new function() {
		var htmlId = '#planet-infobox';
		
		this.show = function() {
			if ($(htmlId).is(':hidden')) $(htmlId).fadeIn();
		}
		
		this.hide = function() {
			if (!$(htmlId).is(':hidden')) $(htmlId).fadeOut();
		}
		
		this.isHidden = function() {
			
		}
		
		this.populate = function(el) {
			var sysFrName = systems.systemFriendlyNames[el.systemName];
			
			var sysData = systems.getSysData(el.systemName.replace(/^system/, ''));
			var bp = el.bulkParameters;
			
			
			if (sysFrName !== 'Sol') {
				var Tstar  = sysData.centerObj.bulkParameters.T;
				var Rstar  = sysData.centerObj.bulkParameters.rxy; // simple radius assumption
				var temp = KeplerFuncts.getTemp(el.bulkParameters.A, el.a, Tstar, Rstar, el.bulkParameters.gh || 0);
			} else {			
				var FE = sysData.centerObj.bulkParameters.FE;
				var Re = 1;
				var R = el.a / Re;
				var A = el.bulkParameters.A; // bond albedo
				var epsilon = 0.9; // assumption
				var temp = KeplerFuncts.getEqTemp(Re, R, FE, A, epsilon, bp.gh);
				temp = temp;
			}
			
			var bulkRatioE = KeplerFuncts.roundPreserve(bp.M / mEarth, 2);
			var bulkRatioJ = KeplerFuncts.roundPreserve(bp.M / mJupiter, 2);
			var bulkRatioS = KeplerFuncts.roundPreserve(bp.M / mSol, 2);
			
			var descItems = {
				genDesc: {
					name: el.name,
					system: sysFrName,
					description: typeof el.description !== 'undefined'? el.description + '\n': 'empty'
				},
			
				items: {
					'diameter': {
						title: 'Width',
						desc: KeplerFuncts.delimit(el.bulkParameters.rxy * 2) + ' km'
					},
					'height': {
						title: 'Height',
						desc: KeplerFuncts.delimit(el.bulkParameters.rz * 2) + ' km'
					},
					'mass': {
						title: 'Mass',
						desc: {
							'masskg': {
								title: 'empty',
								desc: el.bulkParameters.M + ' kg'
							},
							'masspctearth': {
								title: 'empty',
								desc: bulkRatioE + ' earth'
							},
							'masspctjupiter': {
								'title': 'empty',
								desc: bulkRatioJ + ' jupiter'
							},
							'masspctsol': {
								title: 'empty',
								desc: bulkRatioS + ' sun'
							}
						}
					},
					'albedo': {
						title: 'Albedo',
						desc: el.bulkParameters.A + '\n'
					},
					'temp': {
						title: 'Temp',
						desc: {
							'eq': {
								title: 'Average',
								desc: temp + '&deg; celsius'
							},
							'nightside': {
								title: 'Nightside',
								desc: (function() {
									return (typeof bp.nightTemp !== 'undefined')? KeplerFuncts.round(bp.nightTemp - 273.15, 2) + '&deg; celsius' : 'empty';
								})()
							}
						}
					},
					'axial-tilt': {
						title: 'Axial tilt',
						desc: KeplerFuncts.roundPreserve(el.bulkParameters.at / toRad, 3) + ' degrees'
					},
					'solar-distance': {
						title: 'Solar distance',
						desc: {
							'current': {
								title: 'empty',
								desc: '[current distance]'
							},
							'min': {
								title: 'Min',
								desc: KeplerFuncts.roundPreserve(KeplerFuncts.getApsis('peri', el.a, el.b), 3) + ' AU'
							},
							'max': {
								title: 'Max',
								desc: KeplerFuncts.roundPreserve(KeplerFuncts.getApsis('apo', el.a, el.b), 3) + ' AU'
							}
						}
					},
					'orbital-period': {
						title: 'Orbital period',
						desc: {
							'earthdays': {
								title: 'empty',
								desc: KeplerFuncts.delimit(KeplerFuncts.round(el.P, 3)) + ' earth days'
							},
							'earthyears': {
								title: 'empty',
								desc: KeplerFuncts.delimit(KeplerFuncts.round(el.P / 365.25, 3)) + ' earth years'
							}
						}
					}
				},
								
				toHTML: function() {					
					outStr = '<h3>' + descItems.genDesc.name + ' <small>(' + descItems.genDesc.system + ')</small> <a href="#" onclick="systems.planetInfoBox.hide();">[x]</a></h3>\n';
					if (descItems.genDesc.description !== 'empty') outStr += descItems.genDesc.description + '<br/>\n';
					
					outStr += '<ul>\n';
				
					$.each(descItems.items, function(itemId, itemData) {
						var itemTitle = itemData.title !== 'empty'? itemData.title + ': ' : '';
						if (typeof itemData.desc !== 'object' && itemData.desc !== 'empty') outStr += '<li id="pib-' + itemId + '">' + itemTitle + itemData.desc + '\n';
						
						if (typeof itemData.desc === 'object') {
							var outStr1 = '';
							var subItemCtr = 0;
							$.each(itemData.desc, function(subItemId, subItemData) {
								var subItemTitle = subItemData.title !== 'empty'? subItemData.title + ': ' : '';
								if (subItemData.desc !== 'empty') outStr1 += '<li id="pib-' + itemId + '-' + subItemId + '">' + subItemTitle + subItemData.desc + '\n';
							});
							if (outStr1 != '') outStr += '<li>' + itemData.title + '\n<ul>\n' + outStr1 + '</ul>\n';
						}
					});
					outStr += '</ul>';
					
					return outStr;
				}
			}
			
			$(htmlId).find('.pib-content').html(descItems.toHTML());
			
		}
		
	}
}