const J2000JD = 2451545.0;		// J2000 epoch Julian date
const DEFAULT_ITERATIONS = 73;	// Default number of iterations (points) on 'ellipse' lines/meshes

var toRad = 2 * Math.PI / 360; // multiply by toRad to convert degree variables to radians
//var toRad = 2 * Math.PI / 180; // multiply by toRad to convert degree variables to radians

var rEarth = (6378.1 * 2 + 6356.8) / 3;
var mEarth = 5.97219e24;

var rJupiter = (71492 * 2 + 66854) / 3;
var mJupiter = 1.8986e27;

var rSol = 695800;
var mSol = 1.98855e30;

var KeplerSettings = {
	world: {
		backgroundColor: 0xFFFFFF,
		foregroundColor: 0x000000
	},

	orbit: {
		colors: [					// colors to cycle through for each system that is displayed
			0x0000FF,
			0xFF0000,
			0xFFFF00,
			0x00FF00,
			0x00FFFF,
			0xFF00FF,
			0xFFFFFF
		],
		color: 0x0000FF,
		hiliteColor: 0x00FF00,
		showExtendedInfo: true,		// show extended info about orbits
		circApproxFormula: 'google'	// orbit circumference approximation formula
	},
	
	epochDate: J2000JD,				// date to calculate epoch from
	eccIterations: 5,				// iterations for eccentric anomaly calculation
	slowRotation: 5,				// slow rotation of objects by this factor
	simInc: 1,						// simulation increment in earth days
	maxSimRate: 29.94,				// maximum simulation speed (not max framerate)
	scaleFactor: 10,				// scale (enlarging) factor for central objects like stars.
	objScaleFactor: 1e9,			// scale (enlarging) factor for objects. 1 = actual size.
	
	drawAtPeriapsis: false,			// draw all objects at periapsis (for debugging)
	showTextures: false,				// show/hide object textures
	sphereResolution: 8,			// sphere complexity. 8 = fast and blocky, 64 = smooth but more demanding
	orbitEllipseResolution: 54,		// orbit ellipse resolution
	
	animate: true,					// animate objects along orbit
	
	camRadius: 70,					// camera distance from center;
	
	controls: {
		enabled: true,
		type: 'orbit'				// orbit or trackball controls
	},
	
	inclinedView: true,			// default inclined view perspective
	inclinedViewIncline: 20 * toRad, // incline of inclined view in radians if enabled
	defaultSystems: ['sol'],		// default system(s) to load
	
	debug: {
		enabled: true,
		scene: {
			showOrbitAlignmentPlane: false,
			showWorldAxisHelper: true
		},
		objects: {
			showOrbitAxisHelpers: false,
			showObjectAxisHelpers: false
		}
	},
	
	set: function() {
		
	}
};