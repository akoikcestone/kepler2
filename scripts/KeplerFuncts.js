// getJulian: get a Julian date
Date.prototype.getJulian = function() {
    return (this / 86400000) - (this.getTimezoneOffset()/1440) + 2440587.5;
}

function roundFloat(value, places) {
	var placesMult = Math.pow(10, places);
	return Math.round(value * placesMult, places) / placesMult;
}

// keplerFuncts: Kepler utility functions
var KeplerFuncts = new function() {
	// getMeanAnomaly: get a mean anomaly for a given date based on the epoch of the object.
	// date: Date object specifying desired date
	// M0: Mean anomaly of object at epoch
	// epochDate: julian date of epoch
	this.getMeanAnomaly = function(date, M0, P, epochDate) {
		if (typeof epochDate === 'undefined') epochDate = J2000JD;
		
		var n = 2 * Math.PI / P;	// n: mean motion
		var t = date.getJulian() - epochDate;	// t: difference between supplied date and epoch date
		//var t = 0;
		
		var M = M0 + n * t;		  // M: updated mean anomaly
		return M % (2 * Math.PI); // modulus to get M within 0 <= M < 2PI
	}
	
	
	// getTimespan: get a time span between two dates
	// date0, date1: Date objects
	this.getTimespan = function(date0, date1, addText) {
		var timeDiff = date1.getTime() - date0.getTime();
		var timespan = Math.round(timeDiff / 1000 / (3600 * 24));
		if (addText) timespan += ' days';
		
		return timespan;
	}
	

	// convs: conversion factors for distances 'fromto'
	var convs = {
		'lykm': 9460730472580.8,
		'kmly': 1 / 9460730472580.8,
		'lyau': 63241.077,
		'auly': 1 / 63241.077,
		'kmau': 6.68458712e-09,
		'aukm': 1 / 6.68458712e-09,
		'kmmi': 0.621371192,
		'mikm': 1 / 0.621371192
	};
	
	// this.conv: convert distances from one unit to another
	// @quanitity Number number to convert
	// @from String unit to convert from
	// @to String unit to convert to
	// @round Number number of places to round. -1 = no rounding.
	this.conv = function(quantity, from, to, round) {
		if (typeof round === 'undefined') round = -1;
		
		var factor = convs[from + to];
		if (typeof factor === 'undefined') throw 'Conversion factor \'' + from + to + '\' does not exist';
		
		var result = quantity * factor;
		if (round != -1) result = this.round(result, round);
		
		return result;
	}
	
	
	// toHexString: convert js hex Number to string
	// suitable for filling in CSS color values
	// @hexNumber hexadecimal Number
	this.toHexString = function(hexNumber) {
		var hexStr = hexNumber.toString(16);
		var zeroesNeeded = 6 - hexStr.length;
		for (var x = 0; x < zeroesNeeded; x++) hexStr = '0' + hexStr;
		
		return hexStr;
	}
	
	// toHexColor: convert js hex Number to HTML color
	// @hexNumber Number hexadecimal
	// @return String HTML color
	this.toHexColor = function(hexNumber) {
		return '#' + this.toHexString(hexNumber);
	}
	
	
	this.addInfo = function(text) {
		$('#info-bar').html(text);
	}
	
	
	// addDROrbitInfo: add date range info for each orbit
	this.addDROrbitInfo = function(systemName, name, orbitInfo, color) {
		var $systemEl = $('#dr-system-'+systemName);
		if (!$systemEl.length) {
			$('.dr-systems')
				.append('<div class="dr-system" id="dr-system-'+systemName+'" style="background-color: #' + color + '">' + systems.systemFriendlyNames['system'+systemName] + '<div class="dr-system-content"></div></div>')
				.append('<div class="dr-system-pad"></div>');
		}
		
		var orbitInfoHTML = '<ul>';
		$.each(orbitInfo, function(name, value) {
			orbitInfoHTML += '<li>' + name + ': ' + value;
		});
		orbitInfoHTML += '</ul>';
		
		//$('#dr-system-'+systemName).children('.dr-system-content').append('<div class="dr-orbit">' + name + '. ' + value + '</div>');
		$('#dr-system-'+systemName).children('.dr-system-content').append('<div class="dr-orbit">' + name + orbitInfoHTML + '</div>');
	}
	// clearDROrbitInfo - clear date range info
	this.clearDROrbitInfo = function() {
		$('.dr-system').children('.dr-system-content').empty();
	}
	
	// errors: display errors in the error bar
	var errors = [];
	this.error = function(text) {
		if (errors.indexOf(text) != -1) return false;
		
		errors.push(text);
		this.refreshErrors();
	}
	
	this.clearError = function(text) {
		var errIndex = errors.indexOf(text);
		if (errIndex != -1) {
			errors.splice(errIndex);
			this.refreshErrors();
		}
	}
	
	this.refreshErrors = function() {
		var errText = '';
		$.each(errors, function(k, text) {
			errText += text + '<br/>';
		});

		$('#error-bar').html(errText);
	}
	
	
	// show error caused by user in dialog box
	this.userError = function(text) {
		$('#error-dialog').html(text).dialog({modal: true});
	}

	
	// loadDebug: Load debug functions
	this.loadDebug = function() {
		// show offset plane for orbit alignment checks
		if (KeplerSettings.debug.scene.showOrbitAlignmentPlane) {
			//var plgeo = new THREE.PlaneGeometry(50, 50);
			var plgeo = new THREE.BoxGeometry(50, 50, 0.1, 50, 50);
			var plmat = new THREE.MeshBasicMaterial({color: 0x00FF00, overdraw: true});
			var plane = new THREE.Mesh(plgeo, plmat);
			plane.position.z = -1;
			world.scene.add(plane);
		}
	}
	
	
	// round: round to a number of decimal places
	this.round = function(num, places) {
		return Math.round(num * Math.pow(10, places)) / Math.pow(10, places);
	}
	
	
	// this.roundPreserve: preserve rounding of numbers <<< 1 to a number of 'sig figs'
	// @num Number number to round. limit: precision of 20
	// @places Number number of 'sig figs' to preserve
	// @return String rounded number with preserved 'sig figs'
	this.roundPreserve = function(num, places) {
		var numStr = num.toFixed(20);
		var dotIndex = numStr.indexOf('.');
		if (!dotIndex) return num;
		
		var presStr = '';
		var placesCt = 0;
        var placesCtStart = false
		for(var x = dotIndex+1; x < numStr.length; x++) {
			if (placesCt >= places) break;

			presStr += '' + numStr[x];
			if (numStr[x] !== '0') placesCtStart = true;
			if (placesCtStart) placesCt++;
		}
		
		presStr = numStr.substr(0, dotIndex+1) + presStr;
		//return parseFloat(presStr);
        return presStr;
	}
	
	
	// this.getVList: generate correct array of true anomaly increments across the 2*PI limit between a given angle1 and angle2
	// @V0 float angle 1
	// @V1 float angle 2
	// @return Array of increments
	// TODO adjust incBy according to semi-major axis
	this.getVList = function(V0, V1, i) {
		var incBy;
		var increments = typeof i !== 'undefined'? i: KeplerSettings.orbitEllipseResolution;

		var inc = [];
		
		if (V0 > V1) { // break into two segments
			var angle = (V1 + 2 * Math.PI - V0);
			incBy = angle / increments;
			
			var incAccum = 0;
			do {
				var incQuant = V0 + incAccum;
				if (incQuant >= 2 * Math.PI) incQuant = incQuant - 2 * Math.PI;
				inc.push(incQuant);
				
				incAccum += incBy;
			} while (incAccum <= angle);
		} else {
			incBy = (V1 - V0) / increments;
		
			for (var i = V0; i <= V1; i = i + incBy) {
				inc.push(i);
			}
		}
		
		return inc;
	}

	// this.completedOrbits: get number of /complete/ orbits elapsed over a certain time period
	// @dateFrom Date from date
	// @dateTo Date to date
	// @P Number orbital period
	// #todo Incorrect! does not factor in speed changes	
	this.completedOrbits = function(dateFrom, dateTo, P) {
		var jDateDiff = this.getDays(dateFrom, dateTo);
		return Math.floor(jDateDiff / P);
	}
	
	// this.getDays: get number of julian days between two dates
	// @dateFrom Date
	// @dateTo Date
	// @return difference in julian days
	this.getDays = function(dateFrom, dateTo) {
		var jDateFrom = dateFrom.getJulian();
		var jDateTo = dateTo.getJulian();
		
		return jDateTo - jDateFrom;
	}
	
	
	// this.getApsis: get peri/apoapsis
	// @a semi-major axis
	// @b semi-minor axis
	// @return Number peri/apoapsis
	this.getApsis = function(type, a, b) {
		c = Math.sqrt(Math.pow(a, 2) - Math.pow(b,2));
		return type == 'peri'? a - c : a + c;
	}
	
	
	// this.getTemp: get the temperature of an orbiting body
	// @A Number Geometric albedo
	// @D Number distance between star and planet (AU)
	// @Tstar Number temperature of star (K)
	// @Rstar Number radius of star (km)
	// @gh Number greenhouse effect at semi-major axis (K) (optional)
	// @return String temperature of planet (C), delimited
	this.getTemp = function(A, D, Tstar, Rstar, gh) {
		if (typeof gh === 'undefined') gh = 0;
	
		Dkm = D / 6.68458712e-09; // convert distance from AU to km
		tempK = Tstar * Math.pow(1 - A, 0.25) * Math.sqrt(Rstar / (2 * Dkm));
		return this.delimit(this.round(-272.15 + tempK + gh, 2));
	}
	
	
	this.getEqTemp = function(Re, R, FE, A, epsilon, gh) {
		var sigma = 5.67037321e-8;
		var distRatio = Math.pow(Re / R, 2);
		var eqTemp = Math.pow(distRatio * FE * (1 - A) / (4 * epsilon * sigma), 0.25);
		eqTemp = eqTemp - 272.15 + (gh || 0);
		
		return this.delimit(this.round(eqTemp, 2));
	}
	
	
	// this.delimit: delimit a number with commas. Removes decimals smaller than 1e-7
	// @num Number input
	// @return String comma-delimited number
	this.delimit = function(num) {
		// remove very small decimals
		var numStr = num.toString();
		var numFlt = parseFloat(numStr);
		var numFltFloor = Math.floor(numStr);

		var floorDiff = numFlt - numFltFloor;
		if (floorDiff < 1e-7) num = numFltFloor;
		
		// get decimal (tenths and smaller) places
		var decimal = num.toString().match(/\.\d*/) || '';
		
		// ones 'and up' (ones and larger) places
		var onesUp = num >= 0? Math.abs(Math.floor(num)).toString() : Math.abs(Math.ceil(num)).toString();

		var sign = num >= 0? '' : '-';
		var onesUpLen = onesUp.length;
		
		var delimited = '';
		var delim = '';
		var k = 0;
		var thr = 0;
		for (var x = onesUpLen; x >= 0; x--) {
			delim = thr % 3 == 0 && thr != 0 && x != 0? ',' : '';
			delimited = delim + onesUp.substr(x,1) + delimited;
			thr++;
		}
		
		delimited = sign + delimited + decimal;
		
		//$('#div1').append(delimited + '<br/>');
		return delimited;
	}

}