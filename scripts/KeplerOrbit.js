function KeplerOrbit(inElements) {
	var _this = this;
	this.el = inElements;
	
	// setup cache
	this.cache = new Object;
	//this.cache.partialOrbits = [];
	
	// setup parent orbit object
	this.orbitObj = new THREE.Object3D();
	
	// setup variables for orbit
	// input angles must be in radians
	this.setup = function() {
		var els = this.el;
		
		// fill unsupplied variables with default values
		// throw exception if an unsupplied variable is required
		// elementVars: ['name', defaultValue] defaultValue === false means variable is required
		var elementVars = [
			['systemName', false],	// systemName: name of system
			['name', false],		// name: name of object
			['a', false], 			// a: semi-major axis
			['P', false],			// P: period (this can be calculated but not without mass)
			['e', 0], 				// e: eccentricity
			['E', 'notsupplied'], 	// E: eccentric anomaly
			['V', 'notsupplied'], 	// V: true anomaly
			['M', 0], 				// M: mean anomaly
			['M0', 'notsupplied']	// M0: mean anomaly relative to epoch
			['O', 0], 				// O: longitude of ascending node
			['w', 0], 				// w: argument of periapsis
			['i', 0],				// i: inclination
			
			['iterations', 5],		// iterations for Newtonian solution of E given M and e
			['outUnits', 'rad'],		// output units for angles
			['distanceUnits', 'km'],	// distance units
			['round', -1]				// round results
		];
		
		for (var x = 0; x < elementVars.length; x++) {
			var varName      = elementVars[x][0];
			var defaultValue = elementVars[x][1];
			
			if (typeof els[varName] === 'undefined') {
				if (defaultValue === false) throw 'KeplerOrbit.setup exception: Variable required (' + varName + ')';
				els[varName] = defaultValue;
			}
		}
		
		this.orbitObj.name = els.name;
		
		if (els.M0 != 'notsupplied') els.M = KeplerFuncts.getMeanAnomaly(new Date(), els.M0, els.P);
		
		if (els.E == 'notsupplied') els.E = this.getEccAnomaly(els.e, els.M, els.iterations, els.outUnits);
		if (els.V == 'notsupplied') els.V = this.getTrueAnomaly(els.e, els.E, els.outUnits);
		
		if (els.a) els.b = els.a * Math.sqrt(1 - Math.pow(els.e, 2)); // get semi-minor axis
		
		els.r = this.getr(els.a, els.e, els.V);
		els.circ = EllipseLib.arcLen(els.a, els.b, 0, 2 * Math.PI);
	}
	
	
	// get eccentric anomaly
	// M = E - e sin E = E(t) - e sin E(t)
	// E1 = M1 if e < 0.8; PI if e >= 0.8	
	this.getEccAnomaly = function(e, M, iterations) {
		if (typeof iterations === 'undefined') iterations = KeplerSettings.eccIterations;
	
		var E = e < 0.8? M : Math.PI;
		for (var x = 0; x < iterations; x++) {
			E = E - (E - e * Math.sin(E) - M)/(1 - e * Math.cos(E));
		}
		
		return E;
	}
	
	
	// get true anomaly (?)
	// tan(V / 2) = sqrt(1 + e / 1 - e) * tan(E / 2)
	// uses atan2 function to place angle in proper quadrant
	this.getTrueAnomaly = function(e, E) {
		var xarg = Math.sqrt(1 - e) * Math.cos(E/2);
		var yarg = Math.sqrt(1 + e) * Math.sin(E/2);
		V = 2 * Math.atan2(yarg, xarg);
		
		return V;
	}
	
	
	// get eccentric anomaly from true anomaly
	// useful for building orbital ellipses
	// e: eccentricity
	// V: true anomaly
	this.getEccAnomalyFromTrue = function(e, V) {		
		// cos(E) = [cos(?) + e]/[1 + e cos(?)]
		var cosE = (Math.cos(V) + e) / (1 + e * Math.cos(V));
		var E = Math.acos(cosE);
		
		//if (V > Math.PI && V < 2 * Math.PI) E = -E;// + Math.PI;
		if (V > Math.PI) E = -E;// + Math.PI;
		return E;
	}
	

	// getVfromDate: get true anomaly from a date
	// @date Date date
	//
	// useful for getting endpoints to plot an orbit fill wedge / partial orbit indicator
	this.getVfromDate = function(date) {
		var M = KeplerFuncts.getMeanAnomaly(date, this.el.M0, this.el.P, KeplerSettings.epochDate);
		var E = this.getEccAnomaly(this.el.e, M);
		var V = this.getTrueAnomaly(this.el.e, E);
		
		return V;
	}

	
	// getr: get radius from focus (object being orbited)
	// r = a * (1 - e^2) / (1 + e * cos(V)) 
	this.getr = function(a, e, V, update) {
		if (typeof a === 'undefined') a = this.el.a;
		if (typeof e === 'undefined') e = this.el.e;
		if (typeof V === 'undefined') V = this.el.V;
		
		if (typeof update === 'undefined') update = true;
		if (update) {
			V = this.getTrueAnomaly(e, this.el.E);
		}
		
		var r = a * (1 - Math.pow(e, 2)) / (1 + e * Math.cos(V));
		
		return r;
	}
	
	
	// getPos: get 3d position of object for given orbital elements
	this.getPos = function(inElements, cachePosition) {
		if (typeof cachePosition === 'undefined') cachePosition = false;
	
		var els = inElements;
		var a = els.a;
		var e = els.e;
		var E = els.E;
		var w = els.w;
		var i = els.i;
		var O = els.O;
		var round = els.round;
		
		// semi-minor axis of ellipse
		// b = a * sqrt(1 - e^2)
		var b = a * Math.sqrt(1 - Math.pow(e, 2));
		
		// x''', y''', z''' -- first rotation
		var xppp = a * Math.cos(E) - a * e;
		var yppp = b * Math.sin(E);
		var zppp = 0;
		
		// x'', y'' -- rotating z'' by -w
		var xpp = xppp * Math.cos(w) - yppp * Math.sin(w);
		var ypp = xppp * Math.sin(w) + yppp * Math.cos(w);
		var zpp = 0;
		
		// x', y', z' -- rotate x'' by -i
		var xp = xpp;
		var yp = ypp * Math.cos(i);
		var zp = ypp * Math.sin(i);
		
		// x, y, z -- rotate z' by -O
		var x = xp * Math.cos(O) - yp * Math.sin(O);
		var y = xp * Math.sin(O) + yp * Math.cos(O);
		var z = zp;
		
		if (round != -1) {
			var factor = Math.pow(10, round);
			x = Math.round(x * factor) / factor;
			y = Math.round(y * factor) / factor;
			z = Math.round(z * factor) / factor;
		}
		
		var position = [x, y, z];
		
		if (cachePosition) this.cache.position = position;
		
		return position;
	}
	
	
	// getArcLength: get the arc length between any two arbitrary positions on the orbital ellipse
	// @posFrom Array getPosR 'from' position
	// @posTo Array getPosR 'to' position
	this.getArcLength = function(posFrom, posTo, completedOrbits) {
		// offset x values from focus to center of ellipse
		var xOffset = Math.sqrt(Math.pow(this.el.a, 2) - Math.pow(this.el.b, 2));
		posFrom[0] -= xOffset;
		posTo[0] -= xOffset;
		
		// compute length of any full orbit circumferences
		var fullCircs = (completedOrbits >= 1)? completedOrbits * EllipseLib.circApprox(this.el.a, this.el.b, '') : 0;
		var info = '\ngetArcLength';
		info += '\n' + this.el.name;
		info += '\ncompleted orbits: ' + completedOrbits;
		info += '\ndistance of complete orbits: ' + fullCircs;
	
		// compute length of partial orbit arc
		var phiFrom = Math.atan2(posFrom[1], posFrom[0]);
		var phiTo = Math.atan2(posTo[1], posTo[0]);
		
		// correct to positive angular values.
		if (phiFrom < 0) phiFrom += 2 * Math.PI;
		if (phiTo < 0) phiTo += 2 * Math.PI;
		
		info += '\n' + posFrom.join(', ') + '\n' + posTo.join(', ');
		info += '\nfrom: ' + phiFrom / Math.PI + 'pi';
		info += '\nto: ' + phiTo / Math.PI + 'pi';
		
		var partialCirc = EllipseLib.arcLen2(this.el.a, this.el.b, phiFrom, phiTo, undefined, true);
		
		info += '\npartial circ: ' + partialCirc;
		console.log(info);
		
		// return total length (partial arc + any full circumferences)
		return partialCirc + fullCircs;
	}
	
	
	// getArcLengthFromDateRange: get the arc length on the orbital ellipse for a date range
	// @dateFrom Date 'from' date
	// @dateTo Date 'to' date
	this.getArcLengthFromDateRange = function(dateFrom, dateTo) {
		var inElsFrom = this.el;
		inElsFrom.V = this.getVfromDate(dateFrom);
		var posFrom = this.getPosR(inElsFrom, false);
		
		var inElsTo = this.el;
		inElsTo.V = this.getVfromDate(dateTo);
		var posTo = this.getPosR(inElsTo, false);
		
		//var numOrbits = KeplerFuncts.getNumOrbits(dateFrom, dateTo, this.el.P);
		var completedOrbits = KeplerFuncts.completedOrbits(dateFrom, dateTo, this.el.P);
		
		//if (this.el.name == 'Venus') alert(posFrom.join(', ') + '\n' + posTo.join(', '));
		
		return this.getArcLength(posFrom, posTo, completedOrbits);
	}
	
	
	// getPosR: get position for a point along orbital ellipse before being oriented out of the xy plane
	this.getPosR = function(inElements, cachePosition) {
		var r = this.getr(inElements.a, inElements.e, inElements.V, false);
		var rx = r * Math.cos(inElements.V);
		var ry = r * Math.sin(inElements.V);
		var rz = 0;
		
		var position = [rx, ry, rz];
		//alert('r: ' + r + '\n' + 'V: ' + inElements.V + '\n' + position.join(', '));
		if (cachePosition) this.cache.position = position;
		
		return position;
	}
	
	
	
	// this.getOrbitVerticesCoords: get coords of orbit vertices for a range of V0 <= V <= V1 at resolution i
	// @V0 float from V
	// @V1 float to V
	// @i integer iterations (resolution) of line
	// bug when V0 and V1 too close together or V1 < V0
	this.getOrbitVerticesCoords = function(V0, V1, i) {
		if (typeof V0 === 'undefined') V0 = 0;
		if (typeof V1 === 'undefined') V1 = 2 * Math.PI;
		if (typeof i === 'undefined') i = KeplerSettings.orbitEllipseResolution;
		
		if (V0 > V1 && V0 > 2 * Math.PI) {
			return this.cache.fullOrbitCoords;
			//alert(this.el.name + ': V0 is greater than V1.\nV0: ' + V0 + '\nV1: ' + V1);
		}
		
		var inc = 2 * Math.PI / i;
		var verticesCoords = [];
		
		var inElements = $.extend({}, this.el); // duplicate elements to keep this.el.E separate
		
		var vList = KeplerFuncts.getVList(V0, V1, i); // get 2*PI crossover adjusted list of true anomalies
		
		//for (var V = V0; V <= V1; V = V + inc) {
		for (var vInc = 0; vInc < vList.length; vInc++) {
			V = vList[vInc];
		
			var E = this.getEccAnomalyFromTrue(inElements.e, V);
			inElements.E   = E;
			inElements.V   = V;
			//var posElement = [360 * V / 2 / Math.PI];
			//posElement     = posElement.concat(this.getPosR(inElements));
			
			// scale vertices
			var posElement = this.getPosR(inElements);
			var sc = KeplerSettings.scaleFactor;
			posElement[0] = posElement[0] * sc;
			posElement[1] = posElement[1] * sc;
			posElement[2] = posElement[2] * sc;
			
			verticesCoords.push(posElement);
		}
		
		//this.cache[cacheName] = verticesCoords;
		
		return verticesCoords;
	}	
	

	// addOrbit: add orbit and the orbiting object
	this.addOrbit = function() {
		//alert('system color: ' + systems.loadedSystems[this.el.systemName].color);
	
		this.setupOrbitParents();
		this.drawOrbitEllipse(undefined, systems.loadedSystems[this.el.systemName].color);
		this.drawObject(undefined, true);
		
		// add this.orbitObj to system after completely loaded.
		//scene.add(this.orbitObj);
		//alert('systemName: ' + this.el.systemName);
		//console.log(systems.loadedSystems['systemsol']);
		//alert(systems.loadedSystems[this.el.systemName].systemName);
		this.system().add(this.orbitObj);
	}

	
	// addPartialOrbit: add a partial orbit to indicate elapsed distance between two dates
	// @dateFrom Date from date
	// @dateTo Date to date
	this.addPartialOrbit = function(scene, dateFrom, dateTo, erasePrevOrbits) {
		var elapsedTimeHrs = KeplerFuncts.getDays(dateFrom, dateTo) * 24;
		
		var distTraveled = KeplerFuncts.round(this.getArcLengthFromDateRange(dateFrom, dateTo), 3);
		var orbitCirc = KeplerFuncts.round(this.el.circ, 3);
		
		var orbitsCompleted = KeplerFuncts.roundPreserve(distTraveled / orbitCirc, 3); // number of orbits completed based on DISTANCE, not time
		var orbitYears = KeplerFuncts.roundPreserve(KeplerFuncts.getDays(dateFrom, dateTo) / this.el.P, 3);
		var orbitYearsEarth = KeplerFuncts.roundPreserve(KeplerFuncts.getDays(dateFrom, dateTo) / 365, 3);
		var avgSpeed = KeplerFuncts.conv(distTraveled, 'au', 'km', -1) / elapsedTimeHrs;
		var avgSpeedD = KeplerFuncts.delimit(Math.floor(avgSpeed));
		// add orbit info to date range panel

		var V0 = this.getVfromDate(dateFrom);
		var V1 = this.getVfromDate(dateTo);
		
		var vs = {
			elapsedTime0: KeplerFuncts.delimit(KeplerFuncts.getDays(dateFrom, dateTo)),
			elapsedTime1: KeplerFuncts.delimit(KeplerFuncts.round(this.el.P, 3)),
			oY: KeplerFuncts.delimit(orbitYears),
			oYEarth: KeplerFuncts.delimit(orbitYearsEarth),
			oYEarthPlural: orbitYearsEarth != 1? 's' : '',
			distTraveled: KeplerFuncts.delimit(distTraveled),
			dist1Orbit: KeplerFuncts.delimit(orbitCirc),
			oCompleted: KeplerFuncts.delimit(orbitsCompleted),
			avgSpeedcFrac: KeplerFuncts.roundPreserve(avgSpeed / 1079252848.8, 3)
		}
		
		var orbitInfo = {
			// #todo delimit elapsed time
			'Elapsed time': vs.elapsedTime0 + ' / ' + vs.elapsedTime1 + ' days',
			'Orbit years': vs.oY + ' (' + vs.oYEarth + ' earth year' + vs.oYEarthPlural + ')',
			'Distance traveled': vs.distTraveled + ' AU',
			'Distance of one orbit': vs.dist1Orbit + ' AU',
			'Orbits completed': vs.oCompleted,
			'Average speed': avgSpeedD + ' km/h (' + vs.avgSpeedcFrac + 'c)'

		};
		
		var extendedOrbitInfo = {
			'True anomaly from': KeplerFuncts.roundPreserve(V0/Math.PI, 3) + '&pi;',
			'True anomaly to': KeplerFuncts.roundPreserve(V1/Math.PI, 3) + '&pi;'
		};
		
		//if (KeplerSettings.orbit.showExtendedInfo) $.extend(extendedOrbitInfo, orbitInfo);
		if (KeplerSettings.orbit.showExtendedInfo) $.extend(orbitInfo, extendedOrbitInfo);
		
		KeplerFuncts.addDROrbitInfo(
			this.el.systemName.replace(/^system/, ''), 
			this.el.name, 
			//jDateDiff + '/' + KeplerFuncts.round(this.el.P, 3) + 'd (' + jDateRatio + ' orbits)',
			orbitInfo,
			KeplerFuncts.toHexString(systems.loadedSystems[this.el.systemName].color)
		);
		
		var verticesCoords;
		
		//if (this.el.name == 'Ceres') alert('ceres: ' + jDateDiff + '\n' + this.el.P);//alert('ceres: \n' + V0 + '\n' + V1);
		
		// if start and end date encompass whole orbit, return the orbit vertices
		
		if (distTraveled >= orbitCirc) {
			verticesCoords = this.cache.fullOrbitCoords;
		} else {
			//var V0 = this.getVfromDate(dateFrom);
			//var V1 = this.getVfromDate(dateTo);
			
			//if (this.el.name == 'Ceres') alert('ceres: \n' + V0 + '\n' + V1);
			
			verticesCoords = this.getOrbitVerticesCoords(V0, V1, KeplerSettings.orbitEllipseResolution);
		}
		
		this.drawOrbitEllipse(verticesCoords, systems.loadedSystems[this.el.systemName].color, 2, 'partial');
	}
	
	
	// removePartialOrbits: remove all partial orbits
	this.removePartialOrbits = function() {
		this.partialsParent().children = [];	
	}
	
	
	// setupOrbitParents: setup the orbit parent objects to orient orbit correctly
	this.setupOrbitParents = function() {
		// build tree of axis rotation objects
		var oiAxisObj = new THREE.Object3D();
		//oiAxisObj.name = 'oiAxisObj';
		var wAxisObj = new THREE.Object3D();
		//wAxisObj.name = 'wAxisObj';
		wAxisObj.name = this.el.systemName + this.orbitObj.name + 'orbitParent';

		// add parent object for partial orbits
		var partialsParent = new THREE.Object3D();
		partialsParent.name = this.el.systemName + this.orbitObj.name + 'partialsParent';
		wAxisObj.add(partialsParent);
		
		// add w axis object to o,i axis objects and o,i object to orbit object
		oiAxisObj.add(wAxisObj);
		this.orbitObj.add(oiAxisObj);
		
		//wAxisObj.add(new THREE.Line(geometry, material));
		//wAxisObj.add(orbitLine);
		if (KeplerSettings.debug.enabled && KeplerSettings.debug.objects.showOrbitAxisHelpers) wAxisObj.add(new THREE.AxisHelper(10));
		
		oiAxisObj.rotation.order = 'ZYX';
		oiAxisObj.rotation.z = this.el.O;
		oiAxisObj.rotation.y = -this.el.i;
		wAxisObj.rotation.z  = this.el.w;
	}
	
	
	// drawOrbitEllipse: draw orbit ellipse based on supplied list of vertices coords
	// verticesCoords: array of [x, y, z] center-based coordinates
	this.drawOrbitEllipse = function(verticesCoords, color, width, type) {
		if (typeof verticesCoords === 'undefined') verticesCoords = this.getOrbitVerticesCoords(0, Math.PI * 2, KeplerSettings.orbitEllipseResolution, 'primary');
		if (typeof color === 'undefined') color = KeplerSettings.orbit.color;
		if (typeof width === 'undefined') width = 1;
		if (typeof type  === 'undefined') type = 'primary';

		// line material for single-width lines
		var material = new THREE.LineBasicMaterial({color: color});
		var geometry = new THREE.Geometry();
		var vertices = [];

		for (var i=0; i < verticesCoords.length; i++) {
			var vc = verticesCoords[i];
			var vect = new THREE.Vector3(vc[0], vc[1], vc[2]);

			vertices.push(vect);
		}
		
		geometry.vertices = vertices;
		var orbitLine = (width > 1)? ThreedFuncts.drawBoxLines(vertices, width, color) : new THREE.Line(geometry, material);
		
		if (type == 'primary') {
			this.cache.fullOrbitCoords = verticesCoords;
			this.orbitParent(true).add(orbitLine);
		} else if (type == 'partial') {
			this.removePartialOrbits();
			this.partialsParent().add(orbitLine);
			
			// if system is not visible, make sure that added partial orbits are invisible as well
			if (!this.partialsParent().visible) {
				this.partialsParent().traverse(function(obj) {
					obj.visible = false;
				});
			}
		}
	}
	
	
	
	// drawObject: draw object on orbit
	// objCoords: [x, y, z] center-based coordinates of object
	this.drawObject = function(objCoords, initial) {
		if (typeof initial === 'undefined') initial = false;
	
		var inEl = this.el;
		if (KeplerSettings.drawAtPeriapsis || initial) {inEl.E = 0; inEl.V = 0;}
		
		if (typeof objCoords === 'undefined') var objCoords = this.getPosR(inEl, true);
		
		if (initial) {
			// scale radius of object
			var scaledRadius = Math.sqrt(inEl.bulkParameters.rxy) * 6.68458712e-12 * KeplerSettings.objScaleFactor;
		
			var ellipsoidObj  = new THREE.Object3D();
			ellipsoidObj.name = this.el.systemName + this.orbitObj.name + 'object';
			var ellipsoidGeom = new THREE.SphereGeometry(scaledRadius, KeplerSettings.sphereResolution, KeplerSettings.sphereResolution);
			var ellipsoidMat  = new THREE.MeshBasicMaterial({color: 0xFF0000, overdraw: true});
			var ellipsoidMesh = new THREE.Mesh(ellipsoidGeom, ellipsoidMat);
			
			var props = this.el.properties;
			if (typeof props !== 'undefined' && props.objType == 'ellipsoid' && props.texture !== 'undefined' && KeplerSettings.showTextures) {
				ellipsoidMat.map = new THREE.ImageUtils.loadTexture('scripts/models/' + props.texture);
			}
			
			ellipsoidObj.add(ellipsoidMesh);
			if (KeplerSettings.debug.enabled && KeplerSettings.debug.objects.showObjectAxisHelpers) ellipsoidObj.add(new THREE.AxisHelper(5));

			var flattenXY = this.el.bulkParameters.rxy / this.el.bulkParameters.rz;
			if (flattenXY != 1) {ellipsoidObj.scale.x = ellipsoidObj.scale.y = flattenXY;}

			ellipsoidMesh.rotation.x = Math.PI / 2;
			ellipsoidObj.rotation.y = this.el.bulkParameters.at;
			
			this.orbitParent(true).add(ellipsoidObj);
			
			// add onclick event
			this.addEvent(ellipsoidMesh);
		} else {
			ellipsoidObj = this.obj();
		}
		//alert(ellipsoidObj.name);
		
		// apply position
		ellipsoidObj.position.set(objCoords[0] * KeplerSettings.scaleFactor, objCoords[1] * KeplerSettings.scaleFactor, objCoords[2] * KeplerSettings.scaleFactor);	
	}
	
	// add onclick event
	this.addEvent = function(obj) {
		obj.on('click', function() {
			systems.planetInfoBox.populate(_this.el);
			systems.planetInfoBox.show();
		});
		
		obj.on('mouseover', function() {
			$('#' + kepler2CanvasID).css('cursor', 'pointer');
		});
		
		obj.on('mouseout', function() {
			$('#' + kepler2CanvasID).css('cursor', 'auto');
		});
	}	
	
	
	// update the position and rotation of an object.
	this.updObject = function(newCoords, rotInc, objName) {
		var obj = this.obj();
		
		obj.position.set(newCoords[0] * KeplerSettings.scaleFactor, newCoords[1] * KeplerSettings.scaleFactor, newCoords[2] * KeplerSettings.scaleFactor);
		
		// update object rotation
		// alert(rotInc / KeplerSettings.slowRotation);
		obj.rotation.z += rotInc / KeplerSettings.slowRotation;
		
		//console.log('x pos:' + this.cache.objMesh.position.x);
		//if (objName == 'jupiter') alert('inside updObject: \nx = ' + this.cache.objMesh.position.x);
	}
	
	
	// orbitParent: return the parent object of orbit lines, objects, etc.
	this.orbitParent = function(local) {
		if (typeof local === 'undefined') local = false;
		//var orbParent = local? this.orbitObj.children[0].children[0] : world.scene.getObjectByName(this.orbitObj.name).children[0].children[0];
		//return orbParent;
		return local? this.orbitObj.children[0].children[0] : world.scene.getObjectByName(this.el.systemName + this.orbitObj.name + 'orbitParent');
	}
	
	// obj: return parent of object mesh
	this.obj = function() {
		return world.scene.getObjectByName(this.el.systemName + this.orbitObj.name + 'object', true);
	}
	
	// partialsParent: return parent object of all partial orbits. recursively search scene array
	this.partialsParent = function() {
		return world.scene.getObjectByName(this.el.systemName + this.orbitObj.name + 'partialsParent', true);
	}
	
	// system: return parent system of orbit
	this.system = function() {
		return systems.loadedSystems[this.el.systemName].system;
	}
	
	
	// run setup on instantiation
	this.setup();
}
