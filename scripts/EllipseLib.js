var EllipseLib = new function() {

	// arcLen: find the length of an arc spanning an arbitrary length of an ellipse.
	// @a Number semi-major axis
	// @b Number semi-minor axis
	// @phi1 Number start angle from ellipse center
	// @phi2 Number end angle from ellipse center
	// @nIn Number number of iterations (n = 50 gets good accuracy as measured against circApprox 'standard')
	// @return arc length
	this.arcLen = function(a, b, phi1, phi2, nIn, getInfo) {
		nIn = typeof nIn !== 'undefined'?  nIn : 50;
	
		// return circumference if arc spans entire ellipse
		if (phi1 == phi2 || phi2 - phi1 == 2 * Math.PI) return this.circApprox(a, b, '');
	
		// return simple calculation if the ellipse is a circle
		if (a == b) {
			var thFrac = (phi2 - phi1) / (2 * Math.PI);
			return 2 * Math.PI * a * thFrac;
		}
		
		// correct for 'over 2pi'
		if (phi2 < phi1) phi2 = 2 * Math.PI + phi2;
		
		// arc length of ellipse using Simpson's rule. Concept courtesy Joe Bartok
		
		// R and r are inputted as semi (not full) axes
		//var R = a / 2;
		//var r = b / 2;
		var R = a;
		var r = b;
		
		var f1 = phi1;
		var f2 = phi2;
		var n = nIn;
		var df = (f2 - f1) / n;
		var simpsonTerm = 0;
		var sCoeff = 1;
		
		//var sCoeffs = [];
		
		for (var i  = 0; i  <= n; i ++) {
			if (i  == 0 || i  == n) {
				sCoeff = 1;
			} else {
				sCoeff = i % 2 == 0? 2 : 4;
			}
			
			//sCoeffs.push(sCoeff);
			
			var term = sCoeff * Math.sqrt(Math.pow(R * Math.sin(f1 + i * df), 2) + Math.pow(r * Math.cos(f1 + i * df), 2));
			//alert(term);
			simpsonTerm += term;
		}
		
		//console.log(sCoeffs.join(', '));
		//alert(simpsonTerm);
		
		var arclen = Math.abs((df / 3) * simpsonTerm);
		
		if (getInfo) {
			var info = 'phi1: ' + phi1 + '\nphi2: ' + phi2;
			info += '\npct of 2PI: ' + (100 * (phi2 - phi1) / (2 * Math.PI));
			info += '\na: ' + a;
			info += '\narclen: ' + arclen;
			info += '\narclen/total: ' + (100 * arclen / this.circApprox(a, b, ''));
			alert(info);
		}
		
		return arclen;	
	}



	// arcLen2: find the length of an arc spanning an arbitrary length of an ellipse.
	// breaks arc into two segments if 2PI is between start and end
	// @a Number semi-major axis
	// @b Number semi-minor axis
	// @phi1 Number start angle from ellipse center, radians
	// @phi2 Number end angle from ellipse center, radians
	// @nIn Number number of iterations (n = 50 gets good accuracy as measured against circApprox 'standard')
	// @getInfo Boolean output debug info to console
	// @return Number arc length
	this.arcLen2 = function(a, b, phi1, phi2, nIn, getInfo) {
		nIn = typeof nIn !== 'undefined'?  nIn : 50;
	
		// return circumference if arc spans entire ellipse
		if (phi1 == phi2 || phi2 - phi1 == 2 * Math.PI) return this.circApprox(a, b);
	
		// return simple calculation if the ellipse is a circle
		if (a == b) {
			var thFrac = (phi2 - phi1) / (2 * Math.PI);
			return 2 * Math.PI * a * thFrac;
		}
		
		// correct for segments that cross the 2 * PI boundary
		var segs = [];
		if (phi1 > phi2) {
			segs = [[phi1, 2 * Math.PI], [0, phi2]];
		} else {
			segs = [[phi1, phi2]];
		}
		
		// this.arcLen2.segLen: get length of arc on ellipse using Simpson's rule. Concept courtesy Joe Bartok
		// go segment by segment
		// @phiFrom Number 'from' phi, radians
		// @phiTo Number 'to' phi, radians
		this.segLen = function(phiFrom, phiTo) {
			// R and r are inputted as semi (not full) axes
			var R = a;
			var r = b;
			
			var f1 = phiFrom;
			var f2 = phiTo;
			var n = nIn;
			var df = (f2 - f1) / n;
			var simpsonTerm = 0;
			var sCoeff = 1;
			
			for (var i  = 0; i  <= n; i ++) {
				if (i  == 0 || i  == n) {
					sCoeff = 1;
				} else {
					sCoeff = i % 2 == 0? 2 : 4;
				}
				
				var term = sCoeff * Math.sqrt(Math.pow(R * Math.sin(f1 + i * df), 2) + Math.pow(r * Math.cos(f1 + i * df), 2));
				simpsonTerm += term;
			}
			
			var segLen = Math.abs((df / 3) * simpsonTerm);
			
			if (getInfo) {
				var info = 'EllipseLib.arcLen.segLen';
				info += '\nphi1: ' + phiFrom + '\nphi2: ' + phiTo;
				info += '\npct of 2PI: ' + (100 * (phiFrom - phiTo) / (2 * Math.PI));
				info += '\na: ' + a;
				info += '\narclen: ' + segLen;
				info += '\narclen/total: ' + (100 * segLen / this.circApprox(a, b));
				console.log(info);
			}
			
			return segLen;
		}
		
		// return total length of arc segments
		var arcLen = 0;
		for (i = 0; i < segs.length; i++) arcLen += this.segLen(segs[i][0], segs[i][1]);
		return arcLen;	
	}

	
	// circApprox: approximate circumference of an ellipse with given a and b.
	// @a Number semi-major axis
	// @b Number semi-minor axis
	// @return Number approximate circumference of ellipse
	this.circApprox = function(a, b) {
		var output = 0;
	
		switch(KeplerSettings.circApproxFormula) {
			case 'ramanujan':
				output = Math.PI * (3 * (a + b) - Math.sqrt(10 * a * b + 3 * (Math.pow(a, 2) + Math.pow(b, 2))));
				break;
			case 'improved':
				var h = Math.pow(a - b, 2) / Math.pow(a + b, 2);
				output = Math.PI * (a + b) * (1 + (3 * h) / (10 + Math.sqrt(4 - 3 * h)));
				break;
			case 'google':
			default:
				var apb = a + b;
				var amb = a - b;
				var denominator = Math.pow(apb, 2) * (Math.sqrt(-3 * Math.pow(amb, 2) / Math.pow(apb, 2) + 4) + 10);
				
				output = Math.PI * apb;
				output *= 3 * Math.pow(amb, 2) / denominator + 1;
		}
		
		return output;
	}
}