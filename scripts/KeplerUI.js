$(function() {
	var selectedId = '';
	var animTimeout = 500;
	
	$('.menu-item').click(function() {
		var _thisItem = $(this);
	
		if (selectedId == $(this).attr('id')) {
			$('.menu-slide').animate({height: '0'}, animTimeout, function() {
				_thisItem.removeClass('selected');
				selectedId = '';
			});
			
			return false;	// if same button, don't do anything
		}
	
		selectedId = $(this).attr('id');
		var menuVisible = $('.menu-slide').css('height') != '0px';

		var isSel = $(this).hasClass('selected');
		$('.menu-item').removeClass('selected');
		
		isSel? $(this).removeClass('selected') : $(this).addClass('selected');

		if (!menuVisible) {
			$('.menu-slide').animate({height: '220'}, animTimeout, function() {
				
			});
		}
		
		$('.menu-slide > div.panel').hide();
		$('#menu-slide-'+selectedId.substring(5)).show();
	});

	// add date picker for date selection
	$('#date-div').datepicker({
		yearRange: '0:5000',
		changeMonth: true,
		changeYear: true
	});
	
	// add date pickers for date range selection
	$('#date-from-div').datepicker({
		defaultDate: '-1y',
		yearRange: '0:5000',
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
			$('#date-to-div').datepicker('option', 'minDate', selectedDate);
			$('#date-from').val($('#date-from-div').datepicker('getDate'));
		}
	});
	$('#date-to-div').datepicker({
		yearRange: '0:5000',
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
			$('#date-from-div').datepicker('option', 'maxDate', selectedDate);
			$('#date-to').val($('#date-to-div').datepicker('getDate'));
		}
	});
	
	/* settings panel */
	
	// simulation increment slider
	// min-max is range x realtime
	$('#siminc-slider').slider({
		min: 0.1,
		max: 100,
		value: KeplerSettings.simInc,
		step: 0.1,
		slide: function(event, ui) {
			ThreedFuncts.updSimInc(ui.value);
			$('#siminc-slider-factor').html(ui.value + 'x');
		},
		change: function(event, ui) {
			ThreedFuncts.updSimInc(ui.value);
			$('#siminc-slider-factor').html(ui.value + 'x');
		}
	});
});